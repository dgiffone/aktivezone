Introduction
============

**AktiveZone** (from now on referred as AZ) is a minimalistic infrastructure for work distribution in a network of peers (from now on referred to as cluster) that aims to simplicity and efficiency as main goals.

AZ is designed to provide a P2P replacement for the features commonly offered by a broker, when you do not want [the burden of its management and to pay a performance penalty](http://www.zeromq.org/whitepapers:brokerless).

Description
===========

An **active zone** is a cluster identified by a _name_ in which each member is able to determine autonomously the composition of the zone and communicate with other members _asynchronously_ and _efficiently_.

This means that each member can simultaneously play the role of work producer (henceforth **emitter**) and the role of work consumer (henceforth **collector**).

Submitting work to the zone actually consists of sending a payload (of any type) with a routingKey (a string) and a routing strategy.

The available routing strategies are the following:

* ROUND_ROBIN
* RANDOM
* HASH
* FAN_OUT 

The first two strategies are designed to provide canonical load balancing.

The third allows you to target to the same collector all messages that share the same value of the routing key. 
By doing so the collector will receive each message in an orderly fashion. 
This concept can be found in many messaging brokers: e.g. ActiveMQ [messages groups](http://activemq.apache.org/message-groups.html).

By selecting one of the first three strategies the message will be routed to a single collector 
choosen from the actual composition of the zone and the combination of the routing key and routing strategy. 
Think of it as the queue of messaging systems. I.e to an implementation of the the producer-consumer messaging pattern.

The fourth strategy sends the message to all nodes that at that time compose the zone.
Think of it as the topic of messaging systems. I.e. to an implementation of the publish-subscribe messaging pattern.

Features
--------
Key features are: 

*  Service discovery and heartbeat (provided by ZeroConf). 
*  Efficient asynchronous messaging layer (provided by ZeroMQ)

Requirements
------------
TBD

Documentation
-------------
TBD

Installation Instructions
-------------------------
TBD

Usage Overview
--------------

Performance
-----------
Several performance tests are included as JUnit Tests that are run for each build.
Following are some summary results obtained by the two types of zones.

* DirectZone 
20000 MESSAGES (16 bytes) with 2 LISTENERS

                          -------------------------------------------------
                          Elapsed Time (ms)            Throughput (msgs/s) 
          -----------------------------------------------------------------
          Total msgs      Send      Receive            Send         Receive
          -----------------------------------------------------------------
               20000         7           10     2857142.857     2000000.000
          -----------------------------------------------------------------

* BufferingZone
20000 MESSAGES (16 bytes) - 2 WORKERS
    
                          -------------------------------------------------
                          Elapsed Time (ms)            Throughput (msgs/s) 
          -----------------------------------------------------------------
          Total msgs      Send      Receive            Send         Receive
          -----------------------------------------------------------------
               20000         1            9    20000000.000     2222222.222
          -----------------------------------------------------------------

Please note that in the BufferingZone test send throughput is heavily influenced by the usage of the Disruptor RingBuffer.

For the results of the entire test suite, please refer to the [wiki page](https://bitbucket.org/dgiffone/aktivezone.git/wiki/Performance). 

Status
------
**under construction**

License
-------
Apache 2.0

Maintainer Contacts
-------------------
*  Domenico Maria Giffone (domenico.giffone_AT_gmail(dot)com)

