/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone;

import java.io.IOException;

import org.aktivezone.zone.RoutingStrategy;

import com.google.common.base.Stopwatch;

public class App {

	private static final int second = 1000;
	private static final int PORT = 5555;
	private static final String IFACE = "127.0.0.1";
	private static final int BATCH_SIZE = 200000;
	private static final int MSG_SIZE = 64;
	private static final RoutingStrategy STRATEGY = RoutingStrategy.ROUND_ROBIN;

	public static void main(String[] args) throws IOException, InterruptedException {
		String groupName = (args.length == 0 || null == args[0]) ? "testZone" : args[0];
		//int numberOfThreads = (args.length < 2 || null == args[1]) ? 1 : Integer.parseInt(args[1]);
		String iface =  (args.length < 2 || null == args[1]) ? IFACE : args[1];
                int port = (args.length < 3 || null == args[2]) ? PORT : Integer.parseInt(args[2]);
		int batchSize = (args.length < 4 || null == args[3]) ? BATCH_SIZE : Integer.parseInt(args[3]);
		int msgSize = (args.length < 5 || null == args[4]) ? MSG_SIZE : Integer.parseInt(args[4]);
		RoutingStrategy strategy = (args.length < 6 || null == args[5]) ? STRATEGY : RoutingStrategy.values()[Integer.parseInt(args[5])];
		AppTest app = (args.length < 7 || null == args[6]) ? new BufferingTest(iface, groupName, port, batchSize, createDataSize(msgSize), strategy)
				: 1 == Integer.parseInt(args[6]) ? new DirectTest(iface, groupName, port, batchSize, createDataSize(msgSize), strategy) : new BufferingTest(iface, 
						groupName, port, batchSize, createDataSize(msgSize), strategy);
		app.start();
		Runtime.getRuntime().addShutdownHook(new ShutdownThread(app));
		test(app, batchSize);

	}

	private static void test(AppTest app, int batchSize) throws IOException, InterruptedException {
		Stopwatch watch = new Stopwatch();
		Thread.sleep(500);
		System.out.println();
		System.out.println("-- Press s and Enter to Start, CTRL + C to Quit");
		int b;
		while ((b = System.in.read()) != -1 && (char) b != 's')
			;
		watch.start();
		app.test();
		watch.stop();
		printStats("SEND", batchSize, watch);
		test(app, batchSize);
	}

	private static void printStats(String stat, int batchSize, Stopwatch watch) {
		long ms = watch.elapsedMillis();
		double tpt = ((double) batchSize) / ms * second;
		System.out.printf("%n%-20s%n", stat);
		System.out.println("----------------------------------------------------------------------------------");
		System.out.printf("%-20s %-28s %-31s%n", "Total msgs", "Elapsed Time (ms)", "Throughput (msgs/s)");
		System.out.println("----------------------------------------------------------------------------------");
		System.out.printf("%15d %21d %30.3f%n", batchSize, ms, tpt);
		System.out.println("-----------------------------------------------------------------------------------");
		watch.reset();
	}

	private static final class ShutdownThread extends Thread {

		private final AppTest m;

		private ShutdownThread(AppTest mr) {
			super();
			this.m = mr;
		}

		@Override
		public void run() {
			m.stop();
		}
	};

	private static String createDataSize(int msgSize) {
		StringBuilder sb = new StringBuilder(msgSize);
		for (int i = 0; i < msgSize; i++) {
			sb.append('a');
		}
		return sb.toString();
	}

}
