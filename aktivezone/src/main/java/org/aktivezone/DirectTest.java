/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Arrays;

import org.aktivezone.message.handling.PayloadHandler;
import org.aktivezone.message.serializer.Serializer;
import org.aktivezone.zone.DirectZone;
import org.aktivezone.zone.RoutingStrategy;
import org.aktivezone.zone.registry.ZoneRegistry;

import com.google.common.base.Stopwatch;

public class DirectTest implements AppTest {
	
	private static final int second = 1000;
	private static final String STOP_MSG = "STOP";
	private static final String RK = "String";
	private static final byte[] STOP = STOP_MSG.getBytes();
	private String iface;	
	private final String zoneName;
	private final int numberOfThreads;
	private final int port;
	private final int batchSize;
	private final String payload;
	private final RoutingStrategy strategy;
	private ZoneRegistry registry;
	private DirectZone zone;

	public DirectTest(String iface, String groupName, int port, int batchSize, String payload, RoutingStrategy strategy) {
		this.iface = iface;	
		this.zoneName = groupName;
		this.numberOfThreads = 1;
		this.port = port;
		this.batchSize = batchSize;
		this.payload = payload;
		this.strategy = strategy;
	}

	/* (non-Javadoc)
	 * @see org.zerosb.ZBTest#start()
	 */
	@Override
	public void start() throws IOException {
		registry = new ZoneRegistry();
		registry.setLocalhost(iface);
		registry.setZoneName(zoneName);
		registry.setPort(port);
		registry.start();
		zone = new DirectZone(registry, numberOfThreads);
		zone.bind(new Serializer<String>() {
			
			@Override
			public byte[] serialize(String payload) {
				return payload.getBytes();
			}

			@Override
			public String deserialize(byte[] bytes) {
				return new String(bytes);
			}

			@Override
			public Class<String> getHandledType() {
				return String.class;
			}
		}, new PayloadHandler<String>() {
			
			// Single Thread
			private int counter = 0;

			private final Stopwatch receiveWatch = new Stopwatch();

			@Override
			public void handle(String m) {
				if (!receiveWatch.isRunning()) {
					receiveWatch.start();
				}
				counter++;
				if (Arrays.equals(STOP, m.getBytes())) {
					receiveWatch.stop();
					printStats("RECV", counter - 1, receiveWatch);
					receiveWatch.reset();
					counter = 0;
				}
				
			}
		});
		zone.start();
	}
	
	public void test() throws IOException, InterruptedException {
		for (int i = 0; i < batchSize; i++) {
			zone.emit(payload, RK, strategy);
		}
		zone.emit(STOP_MSG, "NO_RK", RoutingStrategy.FAN_OUT);
	}

	/* (non-Javadoc)
	 * @see org.zerosb.ZBTest#stop()
	 */
	@Override
	public void stop() {
		zone.close();
		registry.close();
	}

	private void printStats(String stat, int batchSize, Stopwatch watch) {
		long ms = watch.elapsedMillis();
		double tpt = ((double) batchSize) / ms * second;
		System.out.printf("%n%-20s%n", stat); 
		System.out.println("----------------------------------------------------------------------------------");
		System.out.printf("%-20s %-28s %-31s%n", "Total msgs", "Elapsed Time (ms)", "Throughput (msgs/s)");
		System.out.println("----------------------------------------------------------------------------------");
		System.out.printf("%15d %21d %30.3f%n", batchSize, ms, tpt);
		System.out.println("-----------------------------------------------------------------------------------");
		watch.reset();
	}

}
