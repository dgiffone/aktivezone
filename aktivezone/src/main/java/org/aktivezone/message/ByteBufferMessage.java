/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.aktivezone.zone.RoutingStrategy;

public class ByteBufferMessage implements Message {

	private ByteBuffer buf;

	private byte[] cachedBytes = {};

	@Override
	public byte[] buildMessage(short payloadClassId, byte[] payload, String routingKey, RoutingStrategy strategy, int progressive) {
		init(payloadClassId, payload, routingKey, strategy, progressive);
		return getData();
	}

	public ByteBuffer buildMessageAsBuffer(short classId, byte[] payload, String routingKey, RoutingStrategy strategy, int progressive) {
		init(classId, payload, routingKey, strategy, progressive);
		return getDataAsBuffer();
	}

	private void init(short payloadClassId, byte[] payload, String routingKey, RoutingStrategy strategy, int progressive) {
		clearBuffer();
		setSendTime(buf);
		setSeq(buf, progressive);
		setPayloadTypeId(buf, payloadClassId);
		setStrategy(buf, strategy);
		setRoutingKey(buf, routingKey);
		setPayload(buf, payload);
	}

	public void clearBuffer() {
		buf.clear();
	}

	public ByteBufferMessage() {
	}

	public ByteBufferMessage(byte[] data) {
		init(data);
	}

	public ByteBufferMessage(ByteBuffer byteBuffer) {
		init(byteBuffer);
	}

	public ByteBufferMessage init(ByteBuffer byteBuffer) {
		this.buf = byteBuffer;
		buf.order(ByteOrder.nativeOrder());
		return this;
	}

	public ByteBufferMessage init(byte[] data) {
		this.buf = ByteBuffer.wrap(data).asReadOnlyBuffer();
		buf.order(ByteOrder.nativeOrder());
		return this;
	}

	@Override
	public long getSendTime() {
		return buf.getLong(0);
	}

	private void setSendTime(ByteBuffer bb) {
		bb.putLong(0, System.currentTimeMillis());
	}

	private void setSeq(ByteBuffer bb, int seq) {
		bb.putInt(8, seq);
	}

	@Override
	public int getSeq() {
		return buf.getInt(8);
	}

	private void setPayloadTypeId(ByteBuffer bb, short id) {
		bb.putShort(12, id);
	}

	@Override
	public short getPayloadTypeId() {
		return buf.getShort(12);
	}

	private void setStrategy(ByteBuffer bb, RoutingStrategy strategy) {
		bb.putShort(14, (short) strategy.ordinal());
	}

	@Override
	public RoutingStrategy getStrategy() {
		return RoutingStrategy.values()[buf.getShort(14)];
	}

	private int getRKOffset() {
		return buf.getShort(16) + 18;
	}

	private void setRoutingKey(ByteBuffer bb, String routingKey) {
		short length = (short) routingKey.length();
		bb.putShort(16, length);
		bb.position(18);
		for (int i = 0; i < length; i++)
			bb.put((byte) routingKey.charAt(i));
	}

	@Override
	public byte[] getRoutingKey() {
		short length = buf.getShort(16);
		byte[] values = new byte[length];
		if (length > 0) {
			buf.position(18);
			buf.get(values, 0, length);
			return values;
		}
		return null;
	}

	private void setPayload(ByteBuffer bb, byte[] payload) {
		bb.position(getRKOffset());
		bb.putInt(payload.length);
		bb.put(payload);
	}

	@Override
	public byte[] getPayload() {
		buf.position(getRKOffset());
		int size = buf.getInt();
		byte[] data = new byte[size];
		buf.get(data);
		return data;
	}

	public byte[] getData() {
		buf.flip();
		byte[] bytes = buf.remaining() == cachedBytes.length ? cachedBytes : (cachedBytes = new byte[buf.remaining()]);
		buf.get(bytes);
		return bytes;
	}

	public ByteBuffer getDataAsBuffer() {
		buf.flip();
		buf.position(0);
		return buf.slice();
	}

	public Object getHeader(MessageHeaderKey key) {
		return key.extractValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buf == null) ? 0 : buf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ByteBufferMessage other = (ByteBufferMessage) obj;
		if (buf == null) {
			if (other.buf != null)
				return false;
		} else if (!buf.equals(other.buf))
			return false;
		return true;
	}

}
