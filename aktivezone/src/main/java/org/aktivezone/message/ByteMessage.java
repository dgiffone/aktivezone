/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message;

import java.util.Arrays;

import org.aktivezone.zone.RoutingStrategy;

public class ByteMessage implements Message {

	private static final int SHORT_SIZE = 2;
	private static final int INT_SIZE = 4;

	private static final int SND_TIME_OFFSET = 0;
	private static final int SEQ_OFFSET = 8;
	private static final int PAYLOAD_OFFSET = 12;
	private static final int STGY_OFFSET = 14;
	private static final int RK_LENGTH_OFFSET = 16;
	private static final int RK_OFFSET = RK_LENGTH_OFFSET + SHORT_SIZE;

	private byte[] buf;
	
	private byte[] cachedBytes = {};

	@Override
	public byte[] buildMessage(short payloadClassId, byte[] payload, String routingKey, RoutingStrategy strategy, int progressive) {
		return getData(init(payloadClassId, payload, routingKey, strategy, progressive));
	}

	private int init(short payloadClassId, byte[] payload, String routingKey, RoutingStrategy strategy, int progressive) {
		clearBuffer();
		setSendTime(buf);
		setSeq(buf, progressive);
		setPayloadTypeId(buf, payloadClassId);
		setStrategy(buf, strategy);
		setRoutingKey(buf, routingKey);
		return setPayload(buf, payload);
	}

	public void clearBuffer() {
		buf = new byte[buf.length];
	}

	public ByteMessage() {
	}

	public ByteMessage(byte[] data) {
		init(data);
	}


	public ByteMessage init(byte[] buffer) {
		this.buf = buffer;
		return this;
	}

	private static short getShort(int idx, byte[] src) {
		return (short) ((src[idx] << 8) | (src[idx + 1] & 0xFF));
	}
	
	private static void setShort(int idx, byte[] dst, short value) {
		dst[idx] = (byte) (value >> 8);
		dst[idx + 1] = (byte) value;
	}

	private static void setInt(int idx, byte[] dst, int value) {
		dst[idx] = (byte) (value >> 24);
		dst[idx + 1] = (byte) (value >> 16);
		dst[idx + 2] = (byte) (value >> 8);
		dst[idx + 3] = (byte) value; 
	}
	
	private static int getInt(int idx, byte[] src) {
		return src[idx] << 24 | (src[idx + 1] & 0xFF) << 16 | (src[idx + 2] & 0xFF) << 8 | (src[idx + 3] & 0xFF);
	}

	private static long getLong(int idx, byte[] src) {
		return (src[idx] & 0xFFL) << 56
		        | (src[idx + 1] & 0xFFL) << 48
		        | (src[idx + 2] & 0xFFL) << 40
		        | (src[idx + 3] & 0xFFL) << 32
		        | (src[idx + 4] & 0xFFL) << 24
		        | (src[idx + 5] & 0xFFL) << 16
		        | (src[idx + 6] & 0xFFL) << 8
		        | (src[idx + 7] & 0xFFL);
	}

	private static void setLong(int idx, byte[] dst, final long value) {
		dst[idx] = (byte) (value >> 56);
		dst[idx + 1] = (byte) (value >> 48);
		dst[idx + 2] = (byte) (value >> 40);
		dst[idx + 3] = (byte) (value >> 32);
		dst[idx + 4] = (byte) (value >> 24);
		dst[idx + 5] = (byte) (value >> 16);
		dst[idx + 6] = (byte) (value >> 8);
		dst[idx + 7] = (byte) value;
	}
	
	@Override
	public long getSendTime() {
		   return getLong(SND_TIME_OFFSET, buf);
	}
	
	private void setSendTime(byte[] bb) {
		setLong(SND_TIME_OFFSET, bb, System.currentTimeMillis());
	}
	
	private void setSeq(byte[] bb, int value) {
		setInt(SEQ_OFFSET, bb, value);
	}

	@Override
	public int getSeq() {
		return getInt(SEQ_OFFSET, buf);
	}

	private void setPayloadTypeId(byte[] bb, short value) {
		setShort(PAYLOAD_OFFSET, bb, value);
	}

	@Override
	public short getPayloadTypeId() {
		return getShort(PAYLOAD_OFFSET, buf);
	}

	private void setStrategy( byte[] bb, RoutingStrategy strategy) {
		final short value = (short) strategy.ordinal();
		setShort(STGY_OFFSET, bb, value);
	}

	@Override
	public RoutingStrategy getStrategy() {
		return RoutingStrategy.values()[getShort(STGY_OFFSET, buf)];
	}

	private int getRKOffset() {
		return getShort(RK_LENGTH_OFFSET, buf) + 18;
	}

	private void setRoutingKey(byte[] bb, String routingKey) {
		short length = (short) routingKey.length();
		setShort(RK_LENGTH_OFFSET, bb, length);
		for (int i = 0; i < length; i++)
			bb[RK_OFFSET + i] = ((byte) routingKey.charAt(i));
	}

	@Override
	public byte[] getRoutingKey() {
		short length = getShort(RK_LENGTH_OFFSET, buf);
		byte[] values = new byte[length];
		if (length > 0) {
			System.arraycopy(buf, RK_OFFSET, values, 0, length);
			return values;
		}
		return null;
	}

	private int setPayload( byte[] bb, byte[] payload) {
		final int rkOffset = getRKOffset();
		final int length = payload.length;
		setInt(rkOffset, bb, length);
		System.arraycopy(payload, 0, bb, rkOffset + INT_SIZE, length);
		return rkOffset + INT_SIZE + length;
	}

	@Override
	public byte[] getPayload() {
		final int rkOffset = getRKOffset();
		int size = getInt(rkOffset, buf);
		byte[] data = new byte[size];
		System.arraycopy(buf, rkOffset + INT_SIZE, data, 0, size);
		return data;
	}

	public byte[] getData(int limit) {
		byte[] bytes = limit == cachedBytes.length ? cachedBytes : (cachedBytes = new byte[limit]);
		System.arraycopy(buf, 0, bytes, 0, limit);
		return bytes;
	}

	public Object getHeader(MessageHeaderKey key) {
		return key.extractValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(buf);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ByteMessage other = (ByteMessage) obj;
		if (!Arrays.equals(buf, other.buf))
			return false;
		return true;
	}



}
