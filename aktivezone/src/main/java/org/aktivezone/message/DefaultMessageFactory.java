/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.aktivezone.message.handling.PayloadHandler;
import org.aktivezone.message.serializer.Serializer;
import org.aktivezone.zone.RoutingStrategy;

public class DefaultMessageFactory implements MessageFactory {

	private final Map<Class, Serializer> serializers;
	private final Map<Class, PayloadHandler> handlers;
	private final ArrayList<Class> classes;

	public DefaultMessageFactory() {
		this.serializers = new HashMap<Class, Serializer>();
		this.handlers = new HashMap<Class, PayloadHandler>();
		this.classes = new ArrayList<Class>();
	}

	@Override
	public <T> void bind(Serializer<T> serializer, PayloadHandler<T> payloadHandler) {
		final Class<T> handledType = serializer.getHandledType();
		classes.add(handledType);
		serializers.put(handledType, serializer);
		handlers.put(handledType, payloadHandler);
	}
	
	@Override
	public <T> byte[] build(Message message, T payload, String routingKey, RoutingStrategy strategy, int msgSent) {
		Class clazz = payload.getClass();
		short classId = (short) classes.indexOf(clazz);
		return message.buildMessage(classId, serializers.get(clazz).serialize(payload), routingKey, strategy, msgSent);
	}
	
	@Override
	public void invokeHandler(Message m) {
		final Class expectedType = classes.get(m.getPayloadTypeId());
		final Object payload = expectedType.cast(serializers.get(expectedType).deserialize(m.getPayload()));
		final PayloadHandler handler = handlers.get(expectedType);
		handler.handle(payload);
	}
}
