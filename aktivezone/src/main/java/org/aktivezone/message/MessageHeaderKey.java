/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message;

public enum MessageHeaderKey {
	
	ROUTING_KEY(new ValueExtractor() {
		
		public Object getValue(Message message) {
			return message.getRoutingKey();
		}
	}), 
	
	SEND_TIME(new ValueExtractor() {
		
		public Object getValue(Message message) {
			return message.getSendTime();
		}
	}), 
	
	PROGRESSIVE(new ValueExtractor() {
		
		public Object getValue(Message message) {
			return message.getSeq();
		}
	}), 
	
	REPLY_TO(new ValueExtractor() {
		
		public Object getValue(Message message) {
			return null;
		}
	}),
	PAYLOAD_TYPE(new ValueExtractor() {
		
		public Object getValue(Message message) {
			return message.getPayloadTypeId();
		}
	});

	private MessageHeaderKey(ValueExtractor extractor) {
		this.extractor = extractor;
	}

	public Object extractValue(Message message) {
		return this.extractor.getValue(message);
	}
	
	private ValueExtractor extractor;
	
	private interface ValueExtractor {
		
		Object getValue(Message message);
	}

}
