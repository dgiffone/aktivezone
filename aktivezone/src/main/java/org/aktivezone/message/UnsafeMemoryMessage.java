/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message;

import org.aktivezone.message.serializer.UnsafeMemory;
import org.aktivezone.zone.RoutingStrategy;

public class UnsafeMemoryMessage implements Message {

	private UnsafeMemory buf;

	private byte[] cachedBytes = {};

	@Override
	public byte[] buildMessage(short payloadClassId, byte[] payload, String routingKey, RoutingStrategy strategy, int progressive) {
		init(payloadClassId, payload, routingKey, strategy, progressive);
		return getData();
	}

	private void init(short payloadClassId, byte[] payload, String routingKey, RoutingStrategy strategy, int progressive) {
		clearBuffer();
		setSendTime(buf);
		setSeq(buf, progressive);
		setPayloadTypeId(buf, payloadClassId);
		setStrategy(buf, strategy);
		setRoutingKey(buf, routingKey);
		setPayload(buf, payload);
	}

	public void clearBuffer() {
		buf.reset();
	}

	public UnsafeMemoryMessage() {
	}

	public UnsafeMemoryMessage(byte[] data) {
		init(data);
	}

	public UnsafeMemoryMessage(UnsafeMemory unsafeMemory) {
		init(unsafeMemory);
	}

	public UnsafeMemoryMessage init(UnsafeMemory unsafeMemory) {
		this.buf = unsafeMemory;
		return this;
	}

	public UnsafeMemoryMessage init(byte[] data) {
		this.buf = new UnsafeMemory(data);
		return this;
	}

	@Override
	public long getSendTime() {
		return buf.getLong(0);
	}

	private void setSendTime(UnsafeMemory bb) {
		bb.putLong(0, System.currentTimeMillis());
	}

	private void setSeq(UnsafeMemory bb, int seq) {
		bb.putInt(8, seq);
	}

	@Override
	public int getSeq() {
		return buf.getInt(8);
	}

	private void setPayloadTypeId(UnsafeMemory bb, short id) {
		bb.putShort(12, id);
	}

	@Override
	public short getPayloadTypeId() {
		return buf.getShort(12);
	}

	private void setStrategy(UnsafeMemory bb, RoutingStrategy strategy) {
		bb.putShort(14, (short) strategy.ordinal());
	}

	@Override
	public RoutingStrategy getStrategy() {
		return RoutingStrategy.values()[buf.getShort(14)];
	}

	private int getRKOffset() {
		return buf.getShort(16) + 18;
	}

	private void setRoutingKey(UnsafeMemory bb, String routingKey) {
		short length = (short) routingKey.length();
		bb.putShort(16, length);
		bb.position(18);
		for (int i = 0; i < length; i++)
			bb.putByte((byte) routingKey.charAt(i));
	}

	@Override
	public byte[] getRoutingKey() {
		short length = buf.getShort(16);
		byte[] values = new byte[length];
		if (length > 0) {
			buf.get(values, 18, length);
			return values;
		}
		return null;
	}

	private void setPayload(UnsafeMemory bb, byte[] payload) {
		bb.position(getRKOffset());
		bb.putInt(payload.length);
		bb.put(payload);
	}

	@Override
	public byte[] getPayload() {
		int size = buf.getInt(getRKOffset());
		byte[] data = new byte[size];
		buf.get(data, getRKOffset() + 4, size);
		return data;
	}

	public byte[] getData() {
		byte[] bytes = buf.position() == cachedBytes.length ? cachedBytes : (cachedBytes = new byte[buf.position()]);
		buf.get(bytes);
		return bytes;
	}

	public Object getHeader(MessageHeaderKey key) {
		return key.extractValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buf == null) ? 0 : buf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnsafeMemoryMessage other = (UnsafeMemoryMessage) obj;
		if (buf == null) {
			if (other.buf != null)
				return false;
		} else if (!buf.equals(other.buf))
			return false;
		return true;
	}

}
