/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message.serializer;

import java.nio.ByteBuffer;

/**
 * An ASCII String holder with a max size of 128 chars.
 * All characters are in the ASCII range (0 – 127) to occupy just one  byte in size
 * 
 * @author peter.lawrey
 */
public class AsciiText implements CharSequence {
	private final byte[] values;
	private byte length = 0;

	public AsciiText() {
		this.values = new byte[128];
	}

	@Override
	public int length() {
		return length;
	}

	public char charAt(int index) {
		if (index >= length)
			throw new IndexOutOfBoundsException();
		return (char) (values[index] & 0xFF);
	}

	public CharSequence subSequence(int start, int end) {
		return toString().subSequence(start, end);
	}

	@Override
	public String toString() {
		return length < 0 ? null : new String(values, 0, length);
	}

	public void copyFrom(CharSequence s) {
		if (s instanceof AsciiText) {
			copyFrom((AsciiText) s);
			return;
		}
		if (s == null) {
			length = -1;
			return;
		}
		if (s.length() > values.length)
			throw new IllegalStateException("String too long.");
		length = (byte) s.length();
		for (int i = 0; i < length; i++)
			values[i] = (byte) s.charAt(i);
	}

	public void copyFrom(AsciiText s) {
		length = s.length;
		if (length > 0)
			System.arraycopy(s.values, 0, values, 0, length);
	}

	public void readFrom(ByteBuffer bb) {
		length = bb.get();
		if (length > 0)
			bb.get(values, 0, length);
	}

	public void readFrom(UnsafeMemory bb) {
		length = bb.getByte();
		if (length > 0)
			bb.getByteArray(values);
	}

	public void writeTo(ByteBuffer bb) {
		bb.put(length);
		if (length > 0)
			bb.put(values, 0, length);
	}

	public void writeTo(UnsafeMemory bb) {
		bb.putByte(length);
		if (length > 0)
			bb.putByteArray(values, length);
	}

	@Override
	public int hashCode() {
		int hash = 0;
		for (int i = 0; i < length; i++)
			hash = 31 * hash + (values[i] & 0xFF);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof AsciiText))
			return false;
		if (obj == this)
			return true;
		AsciiText other = (AsciiText) obj;
		if (length != other.length)
			return false;
		for (int i = 0; i < length; i++)
			if (values[i] != other.values[i])
				return false;
		return true;
	}

	public void clear() {
		length = -1;
	}

	public static AsciiText of(CharSequence s) {
		AsciiText text = new AsciiText();
		text.copyFrom(s);
		return text;
	}

	public static AsciiText of(long value) {
		return of(Long.toString(value));
	}

	public static AsciiText of(int value) {
		return of(Integer.toString(value));
	}

	public static AsciiText of(byte value) {
		return of(Byte.toString(value));
	}

	public static AsciiText of(short value) {
		return of(Short.toString(value));
	}

}
