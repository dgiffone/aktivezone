/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message.serializer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.aktivezone.message.serializer.Serializer;

/**
 * This Serializer is NOT Thread Safe because it holds a stateful memory buffer
 * 
 * @author Domenico Maria Giffone.giffone@gmail.com
 * 
 */
public class ByteBufferMessageSerializer //implements MessageSerializer 
{

	/*
	
	private static final int BUFFER_SIZE = 1024;

	private final ByteBuffer bb;
	private final int maxSizeInBytes;

	private byte[] cachedBytes = {};
	

	public ByteBufferMessageSerializer(int maxMessageSizeInBytes) {
		bb = ByteBuffer.allocateDirect(maxMessageSizeInBytes).order(ByteOrder.nativeOrder());
		this.maxSizeInBytes = maxMessageSizeInBytes;
	}

	public ByteBufferMessageSerializer() {
		bb = ByteBuffer.allocateDirect(BUFFER_SIZE).order(ByteOrder.nativeOrder());
		this.maxSizeInBytes = BUFFER_SIZE;
	}
	
	public int getMaxSizeInBytes() {
		return maxSizeInBytes;
	}

	@Override
	public Class<Message> getHandledType() {
		return Message.class;
	}

	
	@Override
	public byte[] serialize(Message payload) {
		bb.clear();
		((MessageImpl)payload).writeTo(bb);
		bb.flip();
		byte[] bytes = bb.remaining() == cachedBytes.length ? cachedBytes : (cachedBytes = new byte[bb.remaining()]);
		bb.get(bytes);
		return bytes;
	}

	@Override
	public MessageImpl deserialize(byte[] bytes) {
		bb.clear();
		bb.put(bytes);
		bb.flip();
		MessageImpl deserialized = new MessageImpl();
		deserialized.readFrom(bb);
		return deserialized;
	} 

	@Override
	public byte[] getRoutingKey(byte[] bytes) {
		throw new IllegalAccessError("Not implemented");
	}
	*/

}
