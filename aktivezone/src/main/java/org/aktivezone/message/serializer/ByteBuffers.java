/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message.serializer;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;


/**
 * @author peter.lawrey
 *
 */
public class ByteBuffers {
	
	private static final Charset ascii;
	private static final Charset utf8;
	private static final Charset utf16;
	
	static {
		ascii = Charset.forName("ISO-8859-1");
		utf8 = Charset.forName("UTF-8");
		utf16 = Charset.forName("UTF-16LE");
	}
	
	private ByteBuffers() {
	}

	public static void read(ByteBuffer bb, AsciiText text) {
		text.readFrom(bb);
	}

	public static int readInt(ByteBuffer bb) {
		int n = bb.get();
		if (n == -128)
			return bb.getInt();
		return n;
	}

	public static long readLong(ByteBuffer bb) {
		int n = bb.get();
		if (n == -128)
			return bb.getLong();
		return n;
	}

	public static void write(ByteBuffer bb, Enum anEnum) {
		bb.put((byte) anEnum.ordinal());
	}

	public static int readOrdinal(ByteBuffer bb) {
		return bb.get() & 0xFF;
	}

	public static void write(ByteBuffer bb, int num) {
		if (num < -127 || num > 127) {
			bb.put((byte) -128);
			bb.putInt(num);
		} else {
			bb.put((byte) num);
		}
	}

	public static void write(ByteBuffer bb, long num) {
		if (num < -127 || num > 127) {
			bb.put((byte) -128);
			bb.putLong(num);
		} else {
			bb.put((byte) num);
		}
	}

	public static void write(ByteBuffer bb, AsciiText text) {
		text.writeTo(bb);
	}
}
