/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message.serializer;


public interface Serializer<T> {
	
	public Class<T> getHandledType();

    /**
     * Serialize the given <code>object</code> into a byte[].
     *
     * @param payload The object to serialize
     * @return the byte array representing the serialized object.
     */
    byte[] serialize(T payload);

    /**
     * Deserialize the first object read from the given <code>bytes</code>. The <code>bytes</code> are not consumed
     * from the array or modified in any way. The resulting object instance is cast to the expected type.
     *
     * @param bytes the bytes providing the serialized data
     * @return the serialized object, cast to the expected type
     *
     * @throws ClassCastException if the first object in the stream is not an instance of &lt;T&gt;.
     */
    T deserialize(byte[] bytes);

}
