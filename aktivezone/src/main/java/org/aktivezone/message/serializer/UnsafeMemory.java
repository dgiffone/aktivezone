/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message.serializer;

import java.lang.reflect.Field;

import sun.misc.Unsafe;

@SuppressWarnings("restriction")
public class UnsafeMemory {
	
	private static final Unsafe unsafe;
	
	static {
		try {
			Field field = Unsafe.class.getDeclaredField("theUnsafe");
			field.setAccessible(true);
			unsafe = (Unsafe) field.get(null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static final long byteArrayOffset = unsafe.arrayBaseOffset(byte[].class);
	private static final long shortArrayOffset = unsafe.arrayBaseOffset(short[].class);
	private static final long longArrayOffset = unsafe.arrayBaseOffset(long[].class);
	private static final long doubleArrayOffset = unsafe.arrayBaseOffset(double[].class);

	private static final int SIZE_OF_BOOLEAN = 1;
	private static final int SIZE_OF_BYTE = 1;
	private static final int SIZE_OF_SHORT = 2;
	private static final int SIZE_OF_INT = 4;
	private static final int SIZE_OF_LONG = 8;

	private int pos = 0;
	private byte[] buffer;

	public UnsafeMemory(final byte[] buffer) {
		if (null == buffer) {
			throw new NullPointerException("buffer cannot be null");
		}

		this.buffer = buffer;
	}

	public void reset() {
		this.pos = 0;
	}
	
	public void init(byte[] bytes) {
		reset();
		this.buffer = bytes;
	}
	
	public void position(final int index) {
		pos = index;
	}
	
	public void putByte(final byte value) {
		unsafe.putByte(buffer, byteArrayOffset + pos, value);
		pos += SIZE_OF_BYTE;
	}
	
	public byte getByte() {
		byte value = unsafe.getByte(buffer, byteArrayOffset + pos);
		pos += SIZE_OF_BYTE;
		
		return value;
	}

	public void putBoolean(final boolean value) {
		unsafe.putBoolean(buffer, byteArrayOffset + pos, value);
		pos += SIZE_OF_BOOLEAN;
	}

	public boolean getBoolean() {
		boolean value = unsafe.getBoolean(buffer, byteArrayOffset + pos);
		pos += SIZE_OF_BOOLEAN;

		return value;
	}
	
	public void putShort(final short value) {
		unsafe.putShort(buffer, shortArrayOffset + pos, value);
		pos += SIZE_OF_SHORT;
	}
	
	public void putShort(final int index, final short value) {
		pos = index;
		unsafe.putShort(buffer, byteArrayOffset + pos, value);
	}

	public short getShort() {
		short value = unsafe.getShort(buffer, byteArrayOffset + pos);
		pos += SIZE_OF_SHORT;

		return value;
	}
	
	public short getShort(final int index) {
		pos = index;
		short value = unsafe.getShort(buffer, byteArrayOffset + pos);

		return value;
	}

	public void putInt(final int value) {
		unsafe.putInt(buffer, byteArrayOffset + pos, value);
		pos += SIZE_OF_INT;
	}
	
	public void putInt(final int index, final int value) {
		pos = index;
		unsafe.putInt(buffer, byteArrayOffset + pos, value);
	}

	public int getInt() {
		int value = unsafe.getInt(buffer, byteArrayOffset + pos);
		pos += SIZE_OF_INT;

		return value;
	}
	
	public int getInt(final int index) {
		pos = index;
		int value = unsafe.getInt(buffer, byteArrayOffset + pos);

		return value;
	}

	public void putLong(final long value) {
		unsafe.putLong(buffer, byteArrayOffset + pos, value);
		pos += SIZE_OF_LONG;
	}
	
	public void putLong(final int index, final long value) {
		pos = index;
		unsafe.putLong(buffer, byteArrayOffset + pos, value);
	}

	public long getLong() {
		long value = unsafe.getLong(buffer, byteArrayOffset + pos);
		pos += SIZE_OF_LONG;

		return value;
	}
	
	public long getLong(final int index) {
		pos = index;
		long value = unsafe.getLong(buffer, byteArrayOffset + pos);
		
		return value;
	}

	public void putLongArray(final long[] values) {
		putInt(values.length);
		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(values, longArrayOffset, buffer, byteArrayOffset + pos, bytesToCopy);
		pos += bytesToCopy;
	}

	public long[] getLongArray() {
		int arraySize = getInt();
		long[] values = new long[arraySize];
		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(buffer, byteArrayOffset + pos, values, longArrayOffset, bytesToCopy);
		pos += bytesToCopy;

		return values;
	}

	public void putDoubleArray(final double[] values) {
		putInt(values.length);
		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(values, doubleArrayOffset, buffer, byteArrayOffset + pos, bytesToCopy);
		pos += bytesToCopy;
	}

	public double[] getDoubleArray() {
		int arraySize = getInt();
		double[] values = new double[arraySize];
		long bytesToCopy = values.length << 3;
		unsafe.copyMemory(buffer, byteArrayOffset + pos, values, doubleArrayOffset, bytesToCopy);
		pos += bytesToCopy;

		return values;
	}
	
	public void putByteArray(final byte[] values) {
		putInt(values.length);
		long bytesToCopy = values.length;
		unsafe.copyMemory(values, byteArrayOffset, buffer, byteArrayOffset + pos, bytesToCopy);
		pos += bytesToCopy;
	}
	
	public void putByteArray(final byte[] values, long bytesToCopy) {
		putLong(bytesToCopy);
		unsafe.copyMemory(values, byteArrayOffset, buffer, byteArrayOffset + pos, bytesToCopy);
		pos += bytesToCopy;
	}
	
	public byte[] getByteArray(final byte[] values) {
		long bytesToCopy =  getLong();
		unsafe.copyMemory(buffer, byteArrayOffset + pos, values, byteArrayOffset, bytesToCopy);
		pos += bytesToCopy;
		
		return values;
	}
	
	public void get(byte[] dst, int offset, int length) {
		unsafe.copyMemory(buffer, byteArrayOffset + offset, dst, byteArrayOffset, length);
		pos = offset + length;
		
	}
	
	public void get(byte[] dst) {
		get(dst, 0, dst.length);
	}
	
	public void put(byte[] src) {
		unsafe.copyMemory(src, byteArrayOffset, buffer, byteArrayOffset + pos, src.length);
		pos += src.length;
		
	}
	
	public void move(long offset) {
		pos+=offset;
	}
	
	public byte[] getByteArray() {
		int arraySize = getInt();
		byte[] values = new byte[arraySize];
		long bytesToCopy = values.length;
		unsafe.copyMemory(buffer, byteArrayOffset + pos, values, byteArrayOffset, bytesToCopy);
		pos += bytesToCopy;

		return values;
	}
	
	public byte[] toByteArray() {
		byte[] values = new byte[pos];
		long bytesToCopy = values.length;
		unsafe.copyMemory(buffer, byteArrayOffset, values, byteArrayOffset, bytesToCopy);
		
		return values;
	}

	public int position() {
		return pos;
	}

}
