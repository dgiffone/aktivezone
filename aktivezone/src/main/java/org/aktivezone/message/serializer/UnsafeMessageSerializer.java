/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message.serializer;

import org.aktivezone.message.serializer.UnsafeMemory;


/**
 * This Serializer is NOT Thread Safe it holds a stateful memory buffer
 * @author Domenico Maria Giffone.giffone@gmail.com
 *
 */
public class UnsafeMessageSerializer //implements MessageSerializer 
{

	/*private static final int BUFFER_SIZE = 1024;
	private final UnsafeMemory buffer;
	
	public UnsafeMessageSerializer() {
		buffer = new UnsafeMemory(new byte[BUFFER_SIZE]);
	}
	
	public UnsafeMessageSerializer(int maxMessageSizeInBytes) {
		buffer = new UnsafeMemory(new byte[maxMessageSizeInBytes]);
	}

	@Override
	public Class<Message> getHandledType() {
		return Message.class;
	}

	
	@Override
	public byte[] serialize(Message payload) {
		buffer.reset();
		((MessageImpl) payload).writeTo(buffer);
		return buffer.toByteArray();
	}

	@Override
	public MessageImpl deserialize(byte[] bytes) {
		buffer.reset(bytes);
		MessageImpl deserialized = new MessageImpl();
		deserialized.readFrom(buffer);
		return deserialized;
	} 
	
	@Override
	public byte[] getRoutingKey(byte[] bytes) {
		buffer.reset(bytes);
		return MessageHeaders.getRoutingKey(buffer);
	}*/

}
