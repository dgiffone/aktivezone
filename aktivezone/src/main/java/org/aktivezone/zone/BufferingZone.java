/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.zone;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.aktivezone.message.ByteBufferMessage;
import org.aktivezone.message.DefaultMessageFactory;
import org.aktivezone.message.handling.PayloadHandler;
import org.aktivezone.message.serializer.Serializer;
import org.aktivezone.zone.registry.ZoneRegistry;
import org.aktivezone.zone.zeromq.Collector;
import org.aktivezone.zone.zeromq.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.lmax.disruptor.ClaimStrategy;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.LifecycleAware;
import com.lmax.disruptor.MultiThreadedClaimStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.SingleThreadedClaimStrategy;
import com.lmax.disruptor.SleepingWaitStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.util.Util;

public class BufferingZone implements Closeable, Zone, MessageDispatcher {

	private static final Logger LOG = LoggerFactory.getLogger(BufferingZone.class);

	private static final WaitStrategy DEFAULT_WAIT_STRATEGY = new SleepingWaitStrategy();

	private static final int MAX_BUFFER_SIZE = 16 * 1024 * 1024;

	private List<Collector> collectors = new ArrayList<Collector>();

	private final ZoneRegistry registry;

	private final ExecutorService executor;

	private final int maxMessageSizeInBytes;

	private final int numberOfWorkers;

	private final int bufferSize;

	private final DefaultMessageFactory messageFactory;

	private WaitStrategy waitStrategy = DEFAULT_WAIT_STRATEGY;

	private volatile Disruptor<CollectEvent> collectorDisruptor;

	private volatile RingBuffer<CollectEvent> collectorBuffer;

	private volatile Disruptor<EmitEvent> emitterDisruptor;

	private volatile RingBuffer<EmitEvent> emitterBuffer;
	
	final CountDownLatch emittersShuttedDown;
	
	final CountDownLatch collectorsShuttedDown;

	private static AtomicInteger emitted = new AtomicInteger();

	private String identity;

	public BufferingZone(ZoneRegistry registry, int numberOfWorker) {
		this(registry, numberOfWorker, 0);
	}

	public BufferingZone(ZoneRegistry registry, int numberOfWorkers, int maxMessageSizeInBytes) {
		super();
		this.registry = registry;
		this.maxMessageSizeInBytes = maxMessageSizeInBytes;
		this.executor = Executors.newFixedThreadPool((numberOfWorkers * 3));
		this.numberOfWorkers = numberOfWorkers;
		this.messageFactory = new DefaultMessageFactory();
		this.bufferSize = MAX_BUFFER_SIZE / maxMessageSizeInBytes;
		this.emittersShuttedDown = new CountDownLatch(1);
		this.collectorsShuttedDown = new CountDownLatch(numberOfWorkers);
	}

	@Override
	public synchronized void start() throws IOException {
		if (emitterDisruptor == null) {
			int sendSize = Util.ceilingNextPowerOfTwo(bufferSize);
			LOG.info("AZ - Creating emit buffer with size of {}", sendSize);
			ClaimStrategy claimStrategy = new MultiThreadedClaimStrategy(sendSize); // emitting is concurrent
			emitterDisruptor = new Disruptor<EmitEvent>(new EmitEvent.Factory(), executor, claimStrategy, waitStrategy);
			emitterDisruptor.handleEventsWith(buildSendEventHandlers(1));
			emitterBuffer = emitterDisruptor.start();
		}
		if (collectorDisruptor == null) {
			int recvSize = Util.ceilingNextPowerOfTwo(bufferSize);
			LOG.info("AZ - Creating collect buffer with size of {}", recvSize);
			ClaimStrategy claimStrategy = new SingleThreadedClaimStrategy(recvSize); // collecting is serial
			collectorDisruptor = new Disruptor<CollectEvent>(new CollectEvent.Factory(), executor, claimStrategy, waitStrategy);
			collectorDisruptor.handleEventsWith(buildRecvEventHandlers(numberOfWorkers));
			collectorBuffer = collectorDisruptor.start();
		}
		final String[] collectorParams = registry.registerCollector();
		final String name = collectorParams[0];
		identity = collectorParams[1];
		LOG.info("AZ - Creating Collector {} for address {}", name, identity);
		Collector collector = new Collector(name, identity, this);
		collectors.add(collector);
		executor.execute(collector);
		collector.waitTilStart();
	}
	
	@Override
	public void close() {
		try {
			LOG.info("AZ - Cleaning up...");
			LOG.info("AZ - Closing emitters...");
			if (emitterDisruptor != null) {
				emitterDisruptor.shutdown();
				this.emittersShuttedDown.await();
			}
			emitterDisruptor = null;
			LOG.info("AZ - Closing collectors...");
			for (Collector collector : collectors) {
				collector.close();
				LOG.debug("{} shutted down.", collector);
			}
			if (collectorDisruptor != null) {
				collectorDisruptor.shutdown();
				this.collectorsShuttedDown.await();
			}
			collectorDisruptor = null;
			if (executor instanceof ExecutorService) {
				LOG.info("AZ - Shutting down threads...");
				((ExecutorService) executor).shutdown();
				if (executor.awaitTermination(30, TimeUnit.SECONDS)) {
					LOG.info("AZ - Cleanup completed.");
				} else {
					LOG.warn("AZ - ExecutorService shutdown sequence didn't complete in 30 sec. Forcing shutdown...");
					((ExecutorService) executor).shutdownNow();
				};
			}
		} catch (Exception e) {
			LOG.error("AZ - Unable perform cleanup due to an exception", e);
		}
	}

	@Override
	public <T> void bind(Serializer<T> serializer, PayloadHandler<T> payloadHandler) {
		messageFactory.bind(serializer, payloadHandler);
	}

	@Override
	public <T> void emit(T payload, String routingKey, RoutingStrategy strategy) {
		final int progressive = emitted.incrementAndGet();
		final String[] recipients = strategy.getRecipients(registry.getDestinations(), routingKey, progressive);
		for (int i = 0; i < recipients.length; i++) {
			final long sequence = emitterBuffer.next();
			final EmitEvent next = emitterBuffer.get(sequence);
			next.clear();
			next.setDestination(recipients[i]);
			next.setPayload(payload);
			next.setRoutingKey(routingKey);
			next.setStrategy(strategy);
			next.setProgressive(progressive);
			emitterBuffer.publish(sequence);
		}
	}

	@Override
	public void dispatch(final byte[] senderIdentity, final byte[] value) {
		final long sequence = collectorBuffer.next();
		final CollectEvent next = collectorBuffer.get(sequence);
		next.clear();
		next.setValue(value);
		collectorBuffer.publish(sequence);
	}

	private final class BatchingCollectEventHandler implements EventHandler<CollectEvent>, LifecycleAware {

		private final String name;
		private final int ordinal;
		private final int numberOfCollectors;
		private static final int MAX_BATCH_SIZE = 100;
		private final ByteBufferMessage[] myArray = new ByteBufferMessage[MAX_BATCH_SIZE];
		private int currentIndex = 0;
		private long collected;

		public BatchingCollectEventHandler(int ordinal, int numberOfCollectors) {
			name = String.format("Collect EH %d/%d", ordinal + 1, numberOfCollectors);
			this.ordinal = ordinal;
			this.numberOfCollectors = numberOfCollectors;
			for (int i = 0; i < myArray.length; i++) {
				myArray[i] = new ByteBufferMessage();
			}
		}

		@Override
		public void onEvent(CollectEvent entry, long sequence, boolean endOfBatch) throws Exception {

			if (currentIndex >= myArray.length) {
				handleBatch(myArray, 0, currentIndex);
				currentIndex = 0;
			}

			myArray[currentIndex].init(entry.getValue());

			if (numberOfWorkers == 1 || ownedByReceiver(myArray[currentIndex].getRoutingKey())) {
				currentIndex++;
			}

			if (endOfBatch) {
				handleBatch(myArray, 0, currentIndex);
				currentIndex = 0;
			}

		}

		private void handleBatch(ByteBufferMessage[] myBatch, int offset, int length) {
			for (int i = offset; i < length; i++) {
				ByteBufferMessage message = myBatch[i];
				messageFactory.invokeHandler(message);
				collected++;
			}
		}

		private boolean ownedByReceiver(byte[] routingKey) {
			return Math.abs(Arrays.hashCode(routingKey) & Integer.MAX_VALUE) % numberOfCollectors == ordinal;
		}

		@Override
		public void onStart() {
			LOG.debug("AZ - {} initialized", name);
		}

		@Override
		public void onShutdown() {
			LOG.info("AZ - {} shutting down...", name);
			LOG.debug("AZ - {} Total msg(s) handled: [{}]", name, collected);
			collectorsShuttedDown.countDown();
		}

		@Override
		public String toString() {
			return name;
		}

	}
	
	private final class StraightCollectEventHandler implements EventHandler<CollectEvent>, LifecycleAware {

		private final String name;
		private final int ordinal;
		private final int numberOfCollectors;
		private final ByteBufferMessage message = new ByteBufferMessage();
		private int currentIndex = 0;
		private long collected;

		public StraightCollectEventHandler(int ordinal, int numberOfCollectors) {
			name = String.format("Collect EH %d/%d", ordinal + 1, numberOfCollectors);
			this.ordinal = ordinal;
			this.numberOfCollectors = numberOfCollectors;
		}

		@Override
		public void onEvent(CollectEvent entry, long sequence, boolean endOfBatch) throws Exception {
			message.init(entry.getValue());
			if (numberOfWorkers == 1 || ownedByReceiver(message.getRoutingKey())) {
				messageFactory.invokeHandler(message);
				collected++;
			}
		}

		private boolean ownedByReceiver(byte[] routingKey) {
			return Math.abs(Arrays.hashCode(routingKey) & Integer.MAX_VALUE) % numberOfCollectors == ordinal;
		}

		@Override
		public void onStart() {
			LOG.debug("AZ - {} initialized", name);
		}

		@Override
		public void onShutdown() {
			LOG.info("AZ - {} shutting down...", name);
			LOG.debug("AZ - {} Total msg(s) handled: [{}]", name, collected);
			collectorsShuttedDown.countDown();
		}

		@Override
		public String toString() {
			return name;
		}

	}

	/*
	 * Batching writer see
	 * http://mechanical-sympathy.blogspot.it/2011/10/smart-batching.html
	 */
	private final class EmitEventHandler implements EventHandler<EmitEvent>, LifecycleAware {

		private static final int BUFFER_SIZE = 16 * 1024; // 16K message
		private static final int MAX_PACKET_SIZE = 49152; // MTU of eth0 is 1500  - lo is 16436

		private final LoadingCache<String, Emitter> emitters = CacheBuilder.newBuilder().expireAfterAccess(10, TimeUnit.SECONDS).initialCapacity(16)
				.removalListener(new RemovalListener<String, Emitter>() {
					public void onRemoval(RemovalNotification<String, Emitter> notification) {
						LOG.debug("AZ - Removing emitter for address {}", notification.getKey());
						notification.getValue().close();
					}

				}).build(new CacheLoader<String, Emitter>() {
					@Override
					public Emitter load(String key) throws Exception {
						LOG.debug("AZ - Creating emitter for address {}", key);
						return new Emitter(identity, key);
					}
				});

		private final String name;
		private final int batch_size; // Smallest payload is 128 byte
		private final Entry[] batch;
		private int currentIndex = 0;
		// private int currentSize = 0;

		private ByteBufferMessage message = new ByteBufferMessage(ByteBuffer.allocateDirect(BUFFER_SIZE));
		private final int ordinal;
		private final int numberOfCollectors;

		public EmitEventHandler(int ordinal, int numberOfCollectors) {
			this.ordinal = ordinal;
			this.numberOfCollectors = numberOfCollectors;
			name = String.format("Emit EH %d/%d", ordinal + 1, numberOfCollectors);
			batch_size = Util.ceilingNextPowerOfTwo(MAX_PACKET_SIZE / maxMessageSizeInBytes);
			batch = new Entry[batch_size];
		}

		@Override
		public void onEvent(EmitEvent entry, long sequence, boolean endOfBatch) throws Exception {
			final String destination = entry.getDestination();
			if (currentIndex >= batch.length) {
				handleBatch(batch, 0, currentIndex);
				currentIndex = 0;
			}
			final byte[] value = messageFactory.build(message, entry.getPayload(), entry.getRoutingKey(), entry.getStrategy(), entry.getProgressive());
			batch[currentIndex].destination = destination;
			batch[currentIndex].value = value;
			currentIndex++;
			if (endOfBatch) {
				handleBatch(batch, 0, currentIndex);
				currentIndex = 0;
			}
		}

		private void handleBatch(Entry[] entries, int offset, int length) {
			Emitter emitter = null;
			String destination = null;
			for (int j = offset; j < length; j++) {
				if (null == destination || !destination.equals(entries[j].destination)) {
					destination = entries[j].destination;
					emitter = emitters.getUnchecked(destination);

				}
				emitter.emit(entries[j].value);
				entries[j].clear();
			}
		}

		private class Entry {
			private String destination;
			private byte[] value;

			private void clear() {
				destination = null;
				value = null;
			}
		}

		private boolean ownedByEmitter(String destination) {
			return Math.abs(destination.hashCode() & Integer.MAX_VALUE) % numberOfCollectors == ordinal;
		}

		@Override
		public void onStart() {
			LOG.debug("AZ - {} initialized with batch size of {}", name, batch_size);
			for (int i = 0; i < batch.length; i++) {
				batch[i] = new Entry();
			}
		}

		@Override
		public void onShutdown() {
			LOG.info("AZ - {} Closing emitters...", name);
			for (String destination : emitters.asMap().keySet()) {
				emitters.asMap().remove(destination);
			}
			message.clearBuffer();
			message = null;
			LOG.debug("AZ - {} clearing {} batch entries ...", name,  batch_size);
			for (int i = 0; i < batch.length; i++) {
				batch[i].clear();
				batch[i] = null;
			}
			emittersShuttedDown.countDown();
		}

	}

	private EventHandler<CollectEvent>[] buildRecvEventHandlers(int numberOfCollectors) {
		EventHandler<CollectEvent>[] processors = new EventHandler[numberOfCollectors];
		for (int ordinal = 0; ordinal < numberOfCollectors; ordinal++) {
			processors[ordinal] = new BatchingCollectEventHandler(ordinal, numberOfCollectors);
		}
		return processors;
	}

	private EventHandler<EmitEvent>[] buildSendEventHandlers(int numberOfCollectors) {
		EventHandler<EmitEvent>[] processors = new EventHandler[numberOfCollectors];
		for (int ordinal = 0; ordinal < numberOfCollectors; ordinal++) {
			processors[ordinal] = new EmitEventHandler(ordinal, numberOfCollectors);
		}
		return processors;
	}

}
