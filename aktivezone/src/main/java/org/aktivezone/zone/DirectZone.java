/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.zone;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.aktivezone.message.ByteBufferMessage;
import org.aktivezone.message.DefaultMessageFactory;
import org.aktivezone.message.Message;
import org.aktivezone.message.handling.PayloadHandler;
import org.aktivezone.message.serializer.Serializer;
import org.aktivezone.zone.registry.ZoneRegistry;
import org.aktivezone.zone.zeromq.Collector;
import org.aktivezone.zone.zeromq.Emitter;
import org.aktivezone.zone.zeromq.ShutdownListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

public class DirectZone implements Zone, MessageDispatcher, Closeable {

	private static final Logger LOG = LoggerFactory.getLogger(Zone.class);

	private List<Collector> collectors = new ArrayList<Collector>();

	private final ZoneRegistry registry;

	private final ExecutorService executor;

	private final DefaultMessageFactory messageFactory;

	private final ThreadLocal<ByteBufferMessage> message;
	
	private final int numberOfWorkers;

	private String identity;

	private static AtomicInteger emitted = new AtomicInteger();

	private final ThreadLocal<LoadingCache<String, Emitter>> emitters; 


	public DirectZone(ZoneRegistry registry, int numberOfWorkers) throws IOException {
		super();
		this.registry = registry;
		this.executor = Executors.newFixedThreadPool(numberOfWorkers);
		this.messageFactory = new DefaultMessageFactory();
		this.message = new ThreadLocal<ByteBufferMessage>() {
			private static final int BUFFER_SIZE = 16 * 1024; // 16K message

			@Override
			protected ByteBufferMessage initialValue() {
				return new ByteBufferMessage(ByteBuffer.allocateDirect(BUFFER_SIZE).order(ByteOrder.nativeOrder()));
			}
			
		};
		this.emitters = new ThreadLocal<LoadingCache<String,Emitter>>() {

			@Override
			protected LoadingCache<String, Emitter> initialValue() {
				return CacheBuilder.newBuilder().expireAfterAccess(10, TimeUnit.SECONDS).initialCapacity(10)
						.removalListener(new RemovalListener<String, Emitter>() {
							public void onRemoval(RemovalNotification<String, Emitter> notification) {
								LOG.debug("AZ - Removing emitter for address {}", notification.getKey());
								notification.getValue().close();
							}
						}).build(new CacheLoader<String, Emitter>() {
							@Override
							public Emitter load(String key) throws Exception {
								LOG.debug("AZ - Creating emitter for address {}", key);
								return new Emitter(identity, key);
							}
						});

			}
			
		};
		this.numberOfWorkers = numberOfWorkers;
	}

	@Override
	public void start() throws IOException {
		for (int i = 0; i < numberOfWorkers; i++) {
			final String[] collectorParams = registry.registerCollector();
			final String name = collectorParams[0];
			String listenAddress = collectorParams[1];
			if (i == 0) {
				identity = listenAddress;
			}
			LOG.debug("AZ - Creating emitter {} for address {}", name, listenAddress);
			Collector collector = new Collector(name, listenAddress, this);
			collector.addShutdownListener(new ShutdownListener() {
				
				@Override
				public void onShutdown() {
					LOG.info("AZ - Shutting down thread local emitters...");
					emitters.get().cleanUp();
					for (String destination : emitters.get().asMap().keySet()) {
						emitters.get().asMap().remove(destination);
					}
				}
			});
			collectors.add(collector);
			executor.execute(collector);
			collector.waitTilStart();
			LOG.debug("{} started.", collector);
		}
	}

	@Override
	public <T> void bind(Serializer<T> serializer, PayloadHandler<T> payloadHandler) {
		messageFactory.bind(serializer, payloadHandler);
	}

	@Override
	public <T> void emit(T payload, String routingKey, RoutingStrategy strategy) {
		final int seq = emitted.incrementAndGet();
		final Message msg = message.get();
		final String[] recipients = msg.getStrategy().getRecipients(registry.getDestinations(), new String(routingKey), seq);
		for (int i = 0; i < recipients.length; i++) {
			final Emitter sender = emitters.get().getUnchecked(recipients[i]);
			final byte[] value = messageFactory.build(msg, payload, routingKey, strategy, seq);
			sender.emit(value);
		}

	}

	@Override
	public void dispatch(final byte[] senderIdentity, final byte[] value) {
		ByteBufferMessage myMsg = message.get().init(value);
		messageFactory.invokeHandler(myMsg);
	}

	@Override
	public void close() {
		try {
			LOG.info("AZ - Cleaning up...");
			LOG.info("AZ - Closing emitters...");
			emitters.get().cleanUp();
			for (String destination : emitters.get().asMap().keySet()) {
				emitters.get().asMap().remove(destination);
			}
			emitters.remove();
			message.remove();
			LOG.info("AZ - Closing collectors...");
			for (Collector collector : collectors) {
				collector.close();
				LOG.debug("{} shutted down.", collector);
			}
			if (executor instanceof ExecutorService) {
				LOG.info("AZ - Shutting down threads...");
				((ExecutorService) executor).shutdown();
				if (executor.awaitTermination(30, TimeUnit.SECONDS)) {
					LOG.info("AZ - Cleanup completed.");
				} else {
					LOG.warn("AZ - ExecutorService shutdown sequence didn't complete in 30 sec. Forcing shutdown...");
					((ExecutorService) executor).shutdownNow();
				};
			}
		} catch (Exception e) {
			LOG.error("AZ - Unable perform cleanup due to an exception", e);
		}
	}

}
