/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.zone;

import com.lmax.disruptor.EventFactory;

public class EmitEvent {

	private Object payload;

	private String routingKey;

	private RoutingStrategy strategy;
	
	private int progressive;

	private String destination;

	static class Factory implements EventFactory<EmitEvent> {
		@Override
		public EmitEvent newInstance() {
			return new EmitEvent();
		}
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}

	public String getRoutingKey() {
		 
		return routingKey;
	}

	public void setRoutingKey(String routingKey) {
		this.routingKey = routingKey;
	}

	public RoutingStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(RoutingStrategy strategy) {
		this.strategy = strategy;
	}

	public void clear() {
		progressive = 0;
		destination = null;
		payload = null;
		routingKey = null;
		strategy = null;
	}

	public int getProgressive() {
		return progressive;
	}

	public void setProgressive(int progressive) {
		this.progressive = progressive;
	}
	
	

}
