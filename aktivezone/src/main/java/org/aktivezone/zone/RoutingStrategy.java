/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.zone;

import java.util.Random;

public enum RoutingStrategy {
	
	ROUND_ROBIN(new RoutingFunction() {
		@Override
		public String[] getRecipients(String[] members, String routingKey, int msgSent) {
			int index = msgSent % members.length;
			return new String[] {members[index]};
		}} ),
	
	RANDOM(new RoutingFunction() {
		@Override
		public String[] getRecipients(String[] members, String routingKey, int msgSent) {
			return new String[] {members[Math.abs(new Random().nextInt(Integer.MAX_VALUE)) % members.length]};
		}} ),
	
	HASH(new RoutingFunction() {
		@Override
		public String[] getRecipients(String[] members, String routingKey, int msgSent) {
			return new String[] {members[Math.abs(routingKey.hashCode() & Integer.MAX_VALUE) % members.length]};
		}} ),
	
	FAN_OUT(new RoutingFunction() {
		@Override
		public String[] getRecipients(String[] members, String routingKey,
				int msgSent) {
			return members;
		}});
	
	private RoutingFunction function;
	
	private RoutingStrategy(RoutingFunction function) {
		this.function = function;
	}

	String[] getRecipients(String[] members, String routingKey, int msgSent) {
		return function.getRecipients(members, routingKey, msgSent);
	}
	
	private interface RoutingFunction {
		String[] getRecipients(String[] members, String routingKey, int msgSent);
	}

}
