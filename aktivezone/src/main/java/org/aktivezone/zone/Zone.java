/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.zone;

import java.io.Closeable;
import java.io.IOException;

import org.aktivezone.message.handling.PayloadHandler;
import org.aktivezone.message.serializer.Serializer;

public interface Zone extends Closeable {
	
	<T> void bind(Serializer<T> serializer, PayloadHandler<T> payloadHandler);
	
	void start() throws IOException;

	<T> void emit(T payload, String routingKey, RoutingStrategy strategy);

}