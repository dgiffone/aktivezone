/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.aktivezone.zone.registry;

import static org.aktivezone.utils.Utils.getNextAvailable;
import static org.aktivezone.utils.Utils.getProcessName;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nullable;

import org.apache.activemq.jmdns.JmDNS;
import org.apache.activemq.jmdns.ServiceEvent;
import org.apache.activemq.jmdns.ServiceInfo;
import org.apache.activemq.jmdns.ServiceListener;
import org.linkedin.zookeeper.client.IZKClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public class ZoneRegistry implements Closeable {

	private static final String PROTO_TCP = "tcp://";

	private static final String PROTO = PROTO_TCP;

	private static final int DEF_PORT = 5555;

	private static final String WORKER_NAME = "worker";

	private static final String ZC_TYPE = "_tcp.local.";

	private static final transient Logger LOG = LoggerFactory.getLogger(ZoneRegistry.class);

	private final AtomicInteger currPort = new AtomicInteger();

	private final AtomicInteger instance = new AtomicInteger();

	private final Map<String, ServiceInfo> serviceInfos = new ConcurrentHashMap<String, ServiceInfo>();

	private Map<String, String> destinations = new ConcurrentHashMap<String, String>();

	private final LoadingCache<InetAddress, JmDNS> mDnsFactory = CacheBuilder.newBuilder().initialCapacity(8)
			.removalListener(new RemovalListener<InetAddress, JmDNS>() {
				public void onRemoval(RemovalNotification<InetAddress, JmDNS> notification) {
					LOG.debug("ZeroConf - Removing JmDNS entry for address {}", notification.getKey());
					notification.getValue().close();
				}

			}).build(new CacheLoader<InetAddress, JmDNS>() {
				@Override
				public JmDNS load(final InetAddress address) throws Exception {
					LOG.debug("ZeroConf - Creating JmDNS entry for address {}", address);
					return new JmDNS(address);
				}
			});

	private JmDNS jmdns;
	private InetAddress localAddress;
	private String localhost;
	private int weight;
	private int priority;
	private IZKClient zkClient;
	private String memberName;
	private int port;
	private String zoneName;

	private String[] availableWorkers;

	public void start() throws IOException {
		getJmdns();

		ServiceListener listener = new ServiceListener() {
			public void serviceAdded(ServiceEvent event) {
				log("added", event);
				JmDNS dns = event.getDNS();
				String type = event.getType();
				String name = event.getName();

				dns.requestServiceInfo(type, name, 1);
			}

			public void serviceRemoved(ServiceEvent event) {
				log("removed", event);
				removeFromServices(event);
			}

			public void serviceResolved(ServiceEvent event) {
				log("resolved", event);
				ServiceInfo info = event.getInfo();
				if (info != null) {
					addToServices(info);
				}
			}

		};

		LOG.info("ZeroConf - Listening for membership notification on zone: {}", getZone());
		jmdns.addServiceListener(getZone(), listener);
	}

	private String getZone() {
		if (null == zoneName) {
			throw new IllegalStateException("Zone Name must be set before start");
		}
		return zoneName + "." + ZC_TYPE;
	}

/**
 */
	public void close() {
		if (jmdns != null) {
			LOG.info("ZeroConf - Shutdown in progress...");
			jmdns.unregisterAllServices();

			final JmDNS target = jmdns;
			Thread thread = new Thread() {
				public void run() {
					try {
						target.close();
						mDnsFactory.asMap().remove(getLocalAddress());
					} catch (Exception e) {
						LOG.error("ZeroConf - Unable to shutdown due to an exception", e);
					}
					LOG.info("ZeroConf - Shutdown completed.");
				}
			};

			thread.setDaemon(true);
			thread.start();

			jmdns = null;
		}
	}

	private ServiceInfo registerService(String memberName, String zoneName, int port) throws IOException {
		ServiceInfo si = createServiceInfo(memberName, port, zoneName);
		registerService(si);
		return si;
	}

	public String[] registerCollector() throws IOException {
		final String[] result = new String[2];
		result[0] = getMemberName();
		ServiceInfo si = registerService(result[0], getZone(), getNextPort());
		result[1] = getDestination(si);
		return result;
	}

	private int getNextPort() {
		currPort.compareAndSet(0, getPort() - 1);
		currPort.set(getNextAvailable(currPort.incrementAndGet()));
		return currPort.get();
	}

	private void registerService(ServiceInfo si) throws IOException {
		getJmdns().registerService(si);
	}

	// Properties
	// -------------------------------------------------------------------------
	public int getPort() {
		if (0 == port) {
			port = DEF_PORT;
		}
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getZoneName() {
		return zoneName;
	}

	public String getMemberName() {
		if (null == memberName) {
			memberName = WORKER_NAME;
		}
		return String.format("%s_%d-%s", memberName, instance.incrementAndGet(), getProcessName());
	}

	public void setMemberName(String name) {
		this.memberName = name;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public JmDNS getJmdns() throws IOException {
		if (jmdns == null) {
			jmdns = createJmDNS();
		}
		return jmdns;
	}

	public void setJmdns(JmDNS jmdns) {
		this.jmdns = jmdns;
	}

	public InetAddress getLocalAddress() throws UnknownHostException {
		if (localAddress == null) {
			localAddress = createLocalAddress();
		}
		return localAddress;
	}

	public void setLocalAddress(InetAddress localAddress) {
		this.localAddress = localAddress;
	}

	public String getLocalhost() {
		return localhost;
	}

	public void setLocalhost(String localhost) {
		this.localhost = localhost;
	}

	public IZKClient getZkClient() {
		return zkClient;
	}

	public void setZkClient(IZKClient zkClient) {
		this.zkClient = zkClient;
	}

	private void addToServices(ServiceInfo info) {
		String destination = getDestination(info);
		serviceInfos.put(info.getName(), info);
		destinations.put(info.getName(), destination);
		updateDests();
	}

	private void updateDests() {
		updateWorkers();
	}

	private String getDestination(ServiceInfo info) {
		StringBuilder sb = new StringBuilder(PROTO);
		sb.append(info.getHostAddress()).append(":").append(info.getPort());
		return sb.toString();
	}

	protected void removeFromServices(ServiceEvent event) {
		destinations.remove(event.getName());
		updateWorkers();
	}

	protected void log(String message, ServiceEvent event) {
		LOG.info("ZeroConf - Member {}: [name='{}', type='{}']", new Object[] { message, event.getName(), event.getType() });
	}

	protected ServiceInfo createServiceInfo(String name, int port, String type) {
		LOG.info("ZeroConf - Registering member: [name='{}', type='{}']", new Object[] { name, type });
		return new ServiceInfo(type, name + "." + type, port, weight, priority, "");
	}

	protected JmDNS createJmDNS() throws IOException {
		InetAddress bindAddress = getLocalAddress();
		LOG.info("ZeroConf - Starting members discovery using inetAddress: {}", bindAddress);
		return mDnsFactory.getUnchecked(bindAddress);
	}

	protected InetAddress createLocalAddress() throws UnknownHostException {
		if (localhost != null) {
			return InetAddress.getByName(localhost);
		}
		return InetAddress.getLocalHost();
	}

	private void updateWorkers() {
		final Iterable<Entry<String, String>> workers = Iterables.filter(destinations.entrySet(), new Predicate<Map.Entry<String, String>>() {
			@Override
			public boolean apply(@Nullable Map.Entry<String, String> input) {
				return input.getKey().startsWith(WORKER_NAME);
			}
		});
		ImmutableList<String> copyOf = ImmutableList.copyOf(Iterables.transform(workers, new Function<Entry<String, String>, String>() {
			@Override
			public String apply(@Nullable Entry<String, String> input) {
				return input.getValue();
			}
		}));
		availableWorkers = copyOf.toArray(new String[copyOf.size()]);
	}

	public String[] getDestinations() {
		return availableWorkers;
	}

}
