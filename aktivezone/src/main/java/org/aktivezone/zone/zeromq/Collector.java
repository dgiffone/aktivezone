/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.aktivezone.zone.zeromq;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.aktivezone.zone.MessageDispatcher;
import org.jeromq.ZMQ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Collector implements Closeable, Runnable {

	private static final Logger LOG = LoggerFactory.getLogger(Collector.class);
	private String listenAddress;
	private String name;
	private boolean running = true;
	private boolean bind = false;
	final CountDownLatch  closed = new CountDownLatch(1);
	final CountDownLatch started = new CountDownLatch(1);
	private final MessageDispatcher messageDispatcher;
	private List<ShutdownListener> shutdownListeners = new ArrayList<ShutdownListener>();
	private ZMQ.Context context;
	private ZMQ.Socket socket;
	private ZMQ.Poller poller;

	public Collector(String name, String listenAddress, MessageDispatcher messageManager) {
		this.name = name;
		this.listenAddress = listenAddress;
		this.messageDispatcher = messageManager;
	}
	
	public void addShutdownListener(ShutdownListener listener) {
		shutdownListeners.add(listener);
	}

	@Override
	public void close() {
		try {
			running = false;
			closed.await();
		} catch (InterruptedException e) {
			LOG.error("AZ - Collector: Exception on close:", e);
		}
	}
	
	public void waitTilStart() {
		try {
			started.await();
		} catch (InterruptedException e) {
			LOG.error("AZ - Collector: Exception on start:", e);
		}
	}

	@Override
	public void run() {
		context = ZMQ.context(1);
		socket = context.socket(ZMQ.ROUTER);
		if (socket != null) {
			socket.setIdentity(name.getBytes());
			LOG.debug("AZ - Collector:  binding name '{}' on address {}", name, listenAddress);
			socket.bind(listenAddress);
			poller = context.poller();
			poller.setAutoClose(false);
			poller.register(socket, ZMQ.Poller.POLLIN);
			bind = true;
			started.countDown();
			while (running) {
				try {
					poller.poll(1000);
					if (poller.pollin(0)) {
						messageDispatcher.dispatch(socket.recv(0), socket.recv(0));
					}
				} catch (Exception e) {
					LOG.error("AZ - Collector: Exception in receive:", e);
					break;
				}
			}
			shutdown();
		}
	}

	private void shutdown() {
		LOG.debug("AZ - Collector: Stopping name '{}' on address {}", name, listenAddress);
		for (ShutdownListener listener : shutdownListeners) {
			listener.onShutdown();
		}
		poller.close();
		socket.close();
		context.term();
		bind = false;
		closed.countDown();
	}

	@Override
	public String toString() {
		return "AZ - Collector: [listenAddress=" + listenAddress + ", name=" + name + ", running=" + running + ", bind=" + bind + "]";
	}

}
