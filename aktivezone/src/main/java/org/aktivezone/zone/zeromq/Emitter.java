/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.zone.zeromq;

import java.io.Closeable;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jeromq.ZMQ;


public class Emitter implements Closeable {

	private static final int NO_FLAG = 0;
	private static final Logger LOG = LoggerFactory.getLogger(Emitter.class);

	private final ZMQ.Socket socket;
	private final ZMQ.Context context;
	private final String destination;
	private final String identity;

	public Emitter(String identity, String recipient) {
		this.destination = recipient;
		this.identity = identity;
		context = ZMQ.context(1);
		socket = context.socket(ZMQ.DEALER);
		socket.setLinger(0);
		socket.setIdentity(identity.getBytes());
		LOG.debug("AZ - Emitter: Connecting to... {} ", recipient);
		socket.connect(recipient);
	}

	public void emit(byte[] bytes) {
		try {
			socket.send(bytes, NO_FLAG);
		} catch (Exception e) {
			LOG.error("AZ - Emitter: Exception in send:", e);
			close();
		}
	}
	
	public void close() {
		socket.close();
		context.term();
		LOG.debug("AZ - Emitter: Shutted down emitter for destination {}", destination);
	}

}
