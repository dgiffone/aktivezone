package org.aktivezone.zone.zeromq;

public interface ShutdownListener {
	
	void onShutdown();

}
