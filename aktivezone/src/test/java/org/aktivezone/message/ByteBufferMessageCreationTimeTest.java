/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message;

import java.nio.ByteBuffer;
import java.util.Arrays;

import junit.framework.Assert;

import org.aktivezone.message.ByteBufferMessage;
import org.aktivezone.message.MessageHeaderKey;
import org.aktivezone.zone.RoutingStrategy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Objects;

public class ByteBufferMessageCreationTimeTest {
	
	private static final String ROUTING_KEY = "RK";

	private static final int ITERATIONS = 1 * 100 * 1000;

	private static final int MESSAGE_SIZE = 16;

	private static final byte[] PAYLOAD = createDataSize(MESSAGE_SIZE).getBytes();

	private static final int BUFFER_SIZE = 16 * 1024;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMessageImplIntByteArrayStringRoutingStrategyInt() throws InterruptedException {
		ByteBufferMessage msg = new ByteBufferMessage(ByteBuffer.allocateDirect(BUFFER_SIZE));
		byte[] data = null;
		for (int i = 0; i < 5; i++) {
			final long startcTimeNanos = System.nanoTime();
			for (int j = 0; j < ITERATIONS; j++) {
				data = msg.buildMessage((short)0, PAYLOAD, ROUTING_KEY, RoutingStrategy.HASH, j);
			}
			final long cTimeNanos = (System.nanoTime() - startcTimeNanos) / ITERATIONS;
			System.out.format("%s\tctime=%,dns\n", "Message Impl Creation Time", cTimeNanos);
			System.gc();
			Thread.sleep(1000);
		}
		msg = new ByteBufferMessage(data);
		for (MessageHeaderKey key : MessageHeaderKey.values()) {
			System.out.println(key + " = " + msg.getHeader(key));
		}
		System.out.println("payload = " + new String(msg.getPayload()));
		Assert.assertTrue((short) 0 ==msg.getPayloadTypeId());
		Assert.assertTrue(Arrays.equals(PAYLOAD, msg.getPayload()));
		Assert.assertEquals(ROUTING_KEY, new String(msg.getRoutingKey()));
	}
	
	private static String createDataSize(int msgSize) {
		StringBuilder sb = new StringBuilder(msgSize);
		char[] chars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		for (int i = 0; i < msgSize; i++) {
			sb.append(chars[i % chars.length]);
		}
		return sb.toString();
	}

}
