/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.aktivezone.message;

import java.nio.ByteBuffer;
import java.util.Arrays;

import junit.framework.Assert;

import org.aktivezone.message.ByteBufferMessage;
import org.aktivezone.message.ByteMessage;
import org.aktivezone.zone.RoutingStrategy;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

public class MessagePassingTest {

	private static final int ITERATIONS = 1 * 100 * 1000;

	private static final int MESSAGE_SIZE = 16;

	protected static final int BUFFER_SIZE = 16 * 1024; // 16K message

	@BeforeClass
	public static void setUp() throws Exception {
//		System.out.println("to Attach Profiler start the jvm with -agentpath:/home/domenico/devel/java/jdt/yjp-11.0.8/bin/linux-x86-64/libyjpagent.so");
		// Thread.sleep(15000);
		// System.out.println("Starting test");
	}

	@After
	public void tearDown() throws Exception {
		System.gc();
		Thread.sleep(1000);
	}

	@Test
	public void DirectByteBufferMessageTest() throws Exception {
		final byte[] payload = createDataSize(MESSAGE_SIZE).getBytes();
		AsymmetricPerformanceTestCase<byte[], ByteBufferMessage> testCase = new AsymmetricPerformanceTestCase<byte[], ByteBufferMessage>(
				"Direct ByteBufferMessage", ITERATIONS, new TestItemFactory<byte[]>() {
					ByteBufferMessage msg = new ByteBufferMessage(ByteBuffer.allocateDirect(BUFFER_SIZE));

					@Override
					public byte[] build() {
						return msg.buildMessage((short) 0, payload, "NO_RK_0", RoutingStrategy.HASH, 1);
					}

				}) {

			byte[] msg = null;

			@Override
			public void testWrite(TestItemFactory<byte[]> factory) throws Exception {
				for (int i = 0; i < ITERATIONS; i++) {
					msg = factory.build();
				}
			}

			@Override
			public ByteBufferMessage testRead() throws Exception {
				ByteBufferMessage deserialized = null;
				for (int i = 0; i < ITERATIONS; i++) {
					deserialized = new ByteBufferMessage(msg);
				}
				return deserialized;
			}
		};
		System.out.println("Direct ByteBufferMessage Serialization And Back");
		for (int i = 0; i < 5; i++) {
			testCase.performTest();
			System.out.format("%s\twrite=%,dns read=%,dns total=%,dns\n", testCase.getName(), testCase.getWriteTimeNanos(), testCase.getReadTimeNanos(),
					testCase.getWriteTimeNanos() + testCase.getReadTimeNanos());
			Assert.assertTrue(Arrays.equals("NO_RK_0".getBytes(), testCase.getTestOutput().getRoutingKey()));
			Assert.assertTrue(Arrays.equals(payload, testCase.getTestOutput().getPayload()));
			System.gc();
			Thread.sleep(1000);
		}
	}

	@Test
	public void HeapByteBufferMessageTest() throws Exception {
		final byte[] payload = createDataSize(MESSAGE_SIZE).getBytes();
		AsymmetricPerformanceTestCase<byte[], ByteBufferMessage> testCase = new AsymmetricPerformanceTestCase<byte[], ByteBufferMessage>(
				"Heap ByteBufferMessage", ITERATIONS, new TestItemFactory<byte[]>() {
					ByteBufferMessage msg = new ByteBufferMessage(ByteBuffer.allocate(BUFFER_SIZE));

					@Override
					public byte[] build() {
						return msg.buildMessage((short) 0, payload, "NO_RK_0", RoutingStrategy.HASH, 1);
					}

				}) {

			byte[] msg = null;

			@Override
			public void testWrite(TestItemFactory<byte[]> factory) throws Exception {
				for (int i = 0; i < ITERATIONS; i++) {
					msg = factory.build();
				}
			}

			@Override
			public ByteBufferMessage testRead() throws Exception {
				ByteBufferMessage deserialized = null;
				for (int i = 0; i < ITERATIONS; i++) {
					deserialized = new ByteBufferMessage(msg);
				}
				return deserialized;
			}
		};
		System.out.println("Heap ByteBufferMessage Serialization And Back");
		for (int i = 0; i < 5; i++) {
			testCase.performTest();
			System.out.format("%s\twrite=%,dns read=%,dns total=%,dns\n", testCase.getName(), testCase.getWriteTimeNanos(), testCase.getReadTimeNanos(),
					testCase.getWriteTimeNanos() + testCase.getReadTimeNanos());
			Assert.assertTrue(Arrays.equals("NO_RK_0".getBytes(), testCase.getTestOutput().getRoutingKey()));
			Assert.assertTrue(Arrays.equals(payload, testCase.getTestOutput().getPayload()));
			System.gc();
			Thread.sleep(1000);
		}
	}

	@Test
	public void ByteMessageTest() throws Exception {
		final byte[] payload = createDataSize(MESSAGE_SIZE).getBytes();
		AsymmetricPerformanceTestCase<byte[], ByteMessage> testCase = new AsymmetricPerformanceTestCase<byte[], ByteMessage>("ByteMessage", ITERATIONS,
				new TestItemFactory<byte[]>() {
					ByteMessage msg = new ByteMessage(new byte[BUFFER_SIZE]);

					@Override
					public byte[] build() {
						return msg.buildMessage((short) 0, payload, "NO_RK_0", RoutingStrategy.HASH, 1);
					}

				}) {

			byte[] msg = null;

			@Override
			public void testWrite(TestItemFactory<byte[]> factory) throws Exception {
				for (int i = 0; i < ITERATIONS; i++) {
					msg = factory.build();
				}
			}

			@Override
			public ByteMessage testRead() throws Exception {
				ByteMessage deserialized = null;
				for (int i = 0; i < ITERATIONS; i++) {
					deserialized = new ByteMessage(msg);
				}
				return deserialized;
			}
		};
		System.out.println("ByteMessage Serialization And Back");
		for (int i = 0; i < 5; i++) {
			testCase.performTest();
			System.out.format("%s\twrite=%,dns read=%,dns total=%,dns\n", testCase.getName(), testCase.getWriteTimeNanos(), testCase.getReadTimeNanos(),
					testCase.getWriteTimeNanos() + testCase.getReadTimeNanos());
			Assert.assertTrue(Arrays.equals("NO_RK_0".getBytes(), testCase.getTestOutput().getRoutingKey()));
			Assert.assertTrue(Arrays.equals(payload, testCase.getTestOutput().getPayload()));
			System.gc();
			Thread.sleep(1000);
		}
	}

	private static String createDataSize(int msgSize) {
		StringBuilder sb = new StringBuilder(msgSize);
		char[] chars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		for (int i = 0; i < msgSize; i++) {
			sb.append(chars[i % chars.length]);
		}
		return sb.toString();
	}

	abstract class AsymmetricPerformanceTestCase<W, R> {
		private final String name;
		private final int repetitions;
		private R testOutput;
		private TestItemFactory<W> testFactory;
		private long writeTimeNanos;
		private long readTimeNanos;

		public AsymmetricPerformanceTestCase(final String name, final int repetitions, final TestItemFactory<W> testFactory) {
			this.name = name;
			this.repetitions = repetitions;
			this.testFactory = testFactory;
		}

		public String getName() {
			return name;
		}

		public R getTestOutput() {
			return testOutput;
		}

		public long getWriteTimeNanos() {
			return writeTimeNanos;
		}

		public long getReadTimeNanos() {
			return readTimeNanos;
		}

		public void performTest() throws Exception {
			final long startWriteNanos = System.nanoTime();
			testWrite(testFactory);
			writeTimeNanos = (System.nanoTime() - startWriteNanos) / repetitions;

			final long startReadNanos = System.nanoTime();
			testOutput = testRead();
			readTimeNanos = (System.nanoTime() - startReadNanos) / repetitions;
		}

		public abstract void testWrite(TestItemFactory<W> factory) throws Exception;

		public abstract R testRead() throws Exception;
	}

	public interface TestItemFactory<T> {

		T build();

	}

}
