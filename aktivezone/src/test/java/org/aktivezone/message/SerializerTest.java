/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.message;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import junit.framework.Assert;

import org.aktivezone.message.serializer.AsciiText;
import org.aktivezone.message.serializer.UnsafeMemory;
import org.aktivezone.zone.RoutingStrategy;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

public class SerializerTest {

	private static final int ITERATIONS = 1 * 100 * 1000;

	private static final int MESSAGE_SIZE = 16;

	private static final AsciiText TXT_ITEM = AsciiText.of(createDataSize(MESSAGE_SIZE));
	private static final String STR_ITEM = createDataSize(MESSAGE_SIZE);
	
	@BeforeClass
	public static void setUp() throws Exception {
//		System.out.println("to Attach Profiler start the jvm with -agentpath:/home/domenico/devel/java/jdt/yjp-11.0.8/bin/linux-x86-64/libyjpagent.so");
//		Thread.sleep(15000);
//		System.out.println("Starting test");
	}

	@After
	public void tearDown() throws Exception {
		System.gc();
		Thread.sleep(1000);
	}

	@Test
	public void AsciiTextByteBufferTest() throws Exception {
		PerformanceTestCase<AsciiText> testCase = new PerformanceTestCase<AsciiText>("ByteBuffer", ITERATIONS, TXT_ITEM) {
			ByteBuffer buffer = ByteBuffer.allocateDirect(1024).order(ByteOrder.nativeOrder());

			@Override
			public void testWrite(AsciiText item) throws Exception {
				for (int i = 0; i < ITERATIONS; i++) {
					buffer.clear();
					item.writeTo(buffer);
				}
			}

			@Override
			public AsciiText testRead() throws Exception {
				AsciiText deserialized = null;
				for (int i = 0; i < ITERATIONS; i++) {
					buffer.flip();
					deserialized = new AsciiText();
					deserialized.readFrom(buffer);
				}
				return deserialized;
			}
		};
		System.out.println("AsciiText Serialization And Back");
		for (int i = 0; i < 5; i++) {
			testCase.performTest();
			System.out.format("%s\twrite=%,dns read=%,dns total=%,dns\n", testCase.getName(), testCase.getWriteTimeNanos(), testCase.getReadTimeNanos(),
					testCase.getWriteTimeNanos() + testCase.getReadTimeNanos());
			Assert.assertEquals(TXT_ITEM, testCase.getTestOutput());
			System.gc();
			Thread.sleep(1000);
		}
	}
	
	@Test
	public void AsciiTextUnsafeTest() throws Exception {
		PerformanceTestCase<AsciiText> testCase = new PerformanceTestCase<AsciiText>("UnsafeMemory", ITERATIONS, TXT_ITEM) {
			UnsafeMemory buffer = new UnsafeMemory(new byte[1024]);
			@Override
			public void testWrite(AsciiText item) throws Exception {
				for (int i = 0; i < ITERATIONS; i++) {
					buffer.reset();
					item.writeTo(buffer);
				}
			}

			@Override
			public AsciiText testRead() throws Exception {
				AsciiText deserialized = null;
				for (int i = 0; i < ITERATIONS; i++) {
					buffer.reset();
					deserialized = new AsciiText();
					deserialized.readFrom(buffer);
				}
				return deserialized;
			}
		};
		System.out.println("AsciiText Serialization And Back");
		for (int i = 0; i < 5; i++) {
			testCase.performTest();
			System.out.format("%s\twrite=%,dns read=%,dns total=%,dns\n", testCase.getName(), testCase.getWriteTimeNanos(), testCase.getReadTimeNanos(),
					testCase.getWriteTimeNanos() + testCase.getReadTimeNanos());
			Assert.assertEquals(TXT_ITEM, testCase.getTestOutput());
			System.gc();
			Thread.sleep(1000);
		}
	}
	
	
	@Test
	public void StringByteBufferTest() throws Exception {
		PerformanceTestCase<String> testCase = new PerformanceTestCase<String>("ByteBuffer", ITERATIONS, STR_ITEM) {
			ByteBuffer buffer = ByteBuffer.allocateDirect(1024).order(ByteOrder.nativeOrder());
			Charset charset = Charset.forName("ISO-8859-1");
			CharsetDecoder decoder = charset.newDecoder();
			CharsetEncoder encoder = charset.newEncoder();


			@Override
			public void testWrite(String item) throws Exception {
				for (int i = 0; i < ITERATIONS; i++) {
					buffer.clear();
					buffer.put(encoder.encode(CharBuffer.wrap(item)));
				}
			}

			@Override
			public String testRead() throws Exception {
				String deserialized = null;
				for (int i = 0; i < ITERATIONS; i++) {
					buffer.flip();
					deserialized = decoder.decode(buffer).toString();
				}
				return deserialized;
			}
		};
		System.out.println("String Serialization And Back");
		for (int i = 0; i < 5; i++) {
			testCase.performTest();
			System.out.format("%s\twrite=%,dns read=%,dns total=%,dns\n", testCase.getName(), testCase.getWriteTimeNanos(), testCase.getReadTimeNanos(),
					testCase.getWriteTimeNanos() + testCase.getReadTimeNanos());
//			Assert.assertEquals(TXT_ITEM, testCase.getTestOutput());
			System.gc();
			Thread.sleep(1000);
		}
	}
	

	private static String createDataSize(int msgSize) {
		StringBuilder sb = new StringBuilder(msgSize);
		char[] chars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		for (int i = 0; i < msgSize; i++) {
			sb.append(chars[i % chars.length]);
		}
		return sb.toString();
	}

	abstract class PerformanceTestCase<T> {
		private final String name;
		private final int repetitions;
		private final T testInput;
		private T testOutput;
		private long writeTimeNanos;
		private long readTimeNanos;

		public PerformanceTestCase(final String name, final int repetitions, final T testInput) {
			this.name = name;
			this.repetitions = repetitions;
			this.testInput = testInput;
		}

		public String getName() {
			return name;
		}

		public T getTestOutput() {
			return testOutput;
		}

		public long getWriteTimeNanos() {
			return writeTimeNanos;
		}

		public long getReadTimeNanos() {
			return readTimeNanos;
		}

		public void performTest() throws Exception {
			final long startWriteNanos = System.nanoTime();
			testWrite(testInput);
			writeTimeNanos = (System.nanoTime() - startWriteNanos) / repetitions;

			final long startReadNanos = System.nanoTime();
			testOutput = testRead();
			readTimeNanos = (System.nanoTime() - startReadNanos) / repetitions;
		}

		public abstract void testWrite(T item) throws Exception;

		public abstract T testRead() throws Exception;
	}

}
