/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.aktivezone.zone;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import org.aktivezone.message.handling.PayloadHandler;
import org.aktivezone.message.serializer.Serializer;
import org.aktivezone.zone.BufferingZone;
import org.aktivezone.zone.RoutingStrategy;
import org.aktivezone.zone.Zone;
import org.aktivezone.zone.registry.ZoneRegistry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

/**
 * remember to set -Djava.library.path=/usr/local/lib/ JVM variable
 * 
 * @author Domenico Maria Giffone
 * 
 */
public class BufferingZoneTest {
	
	private static final int M_PAYLOAD_SIZE = 1024;

	private static final int S_PAYLOAD_SIZE = 512;

	private static final int XS_PAYLOAD_SIZE = 16;

	private static final String ROUTING_KEY = "String";

	private static final int NUM_OF_WORKERS = 1;

	private static final String ZONE_NAME = "_testZone";

	private static final long K = 1024;
	private static final long M = K * K;
	private static final int SECOND_2MS = 1000;
	private static final int DEF_PORT = 5555;
	private static final int DEF_BATCH_SIZE = 20 * 1000;
	private static final int DEF_ITERATIONS = 100;
	private static final boolean SLEEP_BETWEEN_ITERATION = false;

	
	private Stopwatch recvWatch = new Stopwatch();
	private Stopwatch sendWatch = new Stopwatch();
	private CountDownLatch sendLatch;
	private String zoneName;
	private int numberOfWorkers;
	private int port;
	private int batchSize;
	private int msgSize;
	private int iterations;
	private ZoneRegistry registry;
	private Zone zone;
	
	private Serializer<String> stringPayloadSerializer = new Serializer<String>() {

		@Override
		public byte[] serialize(String payload) {
			return payload.getBytes();
		}

		@Override
		public String deserialize(byte[] bytes) {
			return new String(bytes);
		}

		@Override
		public Class<String> getHandledType() {
			return String.class;
		}
	};

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		zone.close();
		registry.close();
		Thread.sleep(3000);
	}

	private void initRegistry() throws IOException {
		registry = new ZoneRegistry();
		registry.setLocalhost("127.0.0.1");
		registry.setZoneName(zoneName);
		registry.setPort(port);
		registry.start();
	}
	
	@Test
	public void testXSMsgWithSingleCollector() throws IOException, InterruptedException {
		this.zoneName = ZONE_NAME;
		this.numberOfWorkers = NUM_OF_WORKERS;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(XS_PAYLOAD_SIZE),XS_PAYLOAD_SIZE + 64);
	}
	
	@Test
	public void testXSMsgWith2Collectors() throws IOException, InterruptedException {
		this.zoneName = ZONE_NAME;
		this.numberOfWorkers = 2;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(XS_PAYLOAD_SIZE), XS_PAYLOAD_SIZE + 64);
	}
	
	@Test
	public void testSMsgWithSingleCollector() throws IOException, InterruptedException {
		this.zoneName = ZONE_NAME;
		this.numberOfWorkers = NUM_OF_WORKERS;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(S_PAYLOAD_SIZE), S_PAYLOAD_SIZE + 64);
	}
	
	@Test
	public void testSMsgWith2Collectors() throws IOException, InterruptedException {
		this.zoneName = ZONE_NAME;
		this.numberOfWorkers = 2;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(S_PAYLOAD_SIZE), S_PAYLOAD_SIZE + 64);
	}
	
	@Test
	public void testMMsgWithSingleCollector() throws IOException, InterruptedException {
		this.zoneName = ZONE_NAME;
		this.numberOfWorkers = NUM_OF_WORKERS;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(M_PAYLOAD_SIZE), M_PAYLOAD_SIZE + 64);
	}
	
	@Test
	public void testMMsgWith2Collectors() throws IOException, InterruptedException {
		this.zoneName = ZONE_NAME;
		this.numberOfWorkers = 2;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(M_PAYLOAD_SIZE), M_PAYLOAD_SIZE + 64);
	}
	
	public void doTest(String payload, int maxMessageInBytes) throws IOException, InterruptedException {
		initRegistry();
		final AtomicBoolean receiving = new AtomicBoolean(false);
		List<PerfItem> perfs = Lists.newArrayList();
		zone = new BufferingZone(registry, numberOfWorkers, maxMessageInBytes);
		zone.bind(stringPayloadSerializer, new PayloadHandler<String>() {

			@Override
			public void handle(String m) {
				if (receiving.compareAndSet(false, true)) {
					recvWatch.start();
				}
				sendLatch.countDown();
			}
		});
		zone.start();
		for (int iter = 0; iter < iterations; iter++) {
			sendLatch = new CountDownLatch(batchSize);
			this.msgSize = payload.getBytes().length;
			sendWatch.start();
			for (int batchItem = 0; batchItem < batchSize; batchItem++) {
				zone.emit(payload, ROUTING_KEY, RoutingStrategy.ROUND_ROBIN);
			}
			sendWatch.stop();
			sendLatch.await();
			recvWatch.stop();
			perfs.add(new PerfItem(sendWatch.elapsedMillis(), recvWatch.elapsedMillis(), batchSize, msgSize));
			sendWatch.reset();
			recvWatch.reset();
			receiving.set(false);
			System.gc();
			if (SLEEP_BETWEEN_ITERATION)
				Thread.sleep(3000);
		}
		printStats(perfs, DEF_ITERATIONS);
	}

	private void printStats(List<PerfItem> items, int iterations) {
		printBanner();
		PerfItem worst = max(items);
		printItem("WORST PERFORMER", worst);
		PerfItem best = min(items);
		printItem("BEST PERFORMER", best);
		items.remove(worst);
		items.remove(best);
		PerfItem median = median(items);
		printItem("MEDIAN", median);
	}

	private void printItem(String measure, PerfItem item) {
		System.out.printf("%n%-40s%n", measure);
		System.out.println("                     ---------------------------------------------------------------------------------");
		System.out.printf("%20s %-28s %-31s %-30s%n", "", "Elapsed Time (ms)", "Throughput (msgs/s)", "Throughput (Mb/s)");
		System.out.println("------------------------------------------------------------------------------------------------------");
		System.out.printf("%15s %9s %12s %15s %15s %15s %15s%n", "Total msgs", "Send", "Receive", "Send", "Receive", "Send", "Receive");
		System.out.println("------------------------------------------------------------------------------------------------------");
		System.out.printf("%15d %9d %12d %15.3f %15.3f %15.3f %15.3f%n", item.batchSize, item.sendMs, item.recvMs, item.sendTpt, item.recvTpt, item.sendMb, item.recvMb);
		System.out.println("------------------------------------------------------------------------------------------------------");
	}
	
	private void printBanner() {
		System.out.println("\n------------------------------------------------------------------------------------------------------");
		System.out.printf("%d ITERATIONS of %d MESSAGES (%d bytes) - %d WORKERS%n", iterations, batchSize, msgSize, numberOfWorkers);
		System.out.println("------------------------------------------------------------------------------------------------------");
	}

	private PerfItem max(List<PerfItem> items) {
		Collections.sort(items);
		return items.get(items.size() - 1);
	}

	private PerfItem min(List<PerfItem> items) {
		Collections.sort(items);
		return items.get(0);
	}

	private PerfItem median(List<PerfItem> values) {
		if (values.size() % 2 == 1)
			return values.get((values.size() + 1) / 2 - 1);
		else {
			PerfItem lower = values.get(values.size() / 2 - 1);
			PerfItem upper = values.get(values.size() / 2);

			return new PerfItem((lower.sendMs + upper.sendMs) / 2, 
					(lower.recvMs + upper.recvMs) / 2, 
					lower.batchSize, lower.msgSize);
		}
	}

	private static final class PerfItem implements Comparable<PerfItem> {
		 long sendMs;
		 long recvMs;
		 double sendTpt;
		 double recvTpt;
		 double sendMb;
		 double recvMb;
		 int batchSize;
		 int msgSize;

		PerfItem(long sendWatchMs, long recvWatchMs, int batchSize, int msgSize) {
			this.sendMs = sendWatchMs;
			this.recvMs = recvWatchMs;
			this.sendTpt = ((double) batchSize) / sendMs * SECOND_2MS;
			this.recvTpt = ((double) batchSize) / recvMs * SECOND_2MS;
			this.sendMb = (sendTpt * ((double) msgSize) * 8 / M);
			this.recvMb = (recvTpt * ((double) msgSize) * 8 / M);
			this.batchSize = batchSize;
			this.msgSize = msgSize;
		}

		@Override
		public int compareTo(PerfItem o) {
			return Long.valueOf(sendMs + recvMs).compareTo(Long.valueOf(o.sendMs + o.recvMs));
		}

	}
	
	private static String createDataSize(int msgSize) {
		  StringBuilder sb = new StringBuilder(msgSize);
		  for (int i=0; i<msgSize; i++) {
		    sb.append('a');
		  }
		  return sb.toString();
		}

}
