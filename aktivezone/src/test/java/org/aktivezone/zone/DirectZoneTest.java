/*
 * Copyright (c) 2012 Domenico Maria Giffone 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.aktivezone.zone;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import org.aktivezone.message.handling.PayloadHandler;
import org.aktivezone.message.serializer.Serializer;
import org.aktivezone.zone.DirectZone;
import org.aktivezone.zone.RoutingStrategy;
import org.aktivezone.zone.Zone;
import org.aktivezone.zone.registry.ZoneRegistry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

/**
 * remember to set -Djava.library.path=/usr/local/lib/ JVM variable
 * 
 * @author Domenico Maria Giffone
 * 
 */
public class DirectZoneTest {

	private static final int M_PAYLOAD_SIZE = 1024;

	private static final int S_PAYLOAD_SIZE = 512;

	private static final int XS_PAYLOAD_SIZE = 16;

	private static final String ROUTING_KEY = "RK0";

	private static final String ZONE_NAME = "_testZone";

	private static final long K = 1024;
	private static final long M = K * K;
	private static final int SECOND_2MS = 1000;
	private static final int DEF_PORT = 5555;
	private static final int DEF_BATCH_SIZE = 20 * 1000;
	private static final int DEF_ITERATIONS = 100;
	private static final boolean SLEEP_BETWEEN_ITERATION = false;

	private Stopwatch collectWatch = new Stopwatch();
	private Stopwatch emitWatch = new Stopwatch();
	private CountDownLatch emitLatch;
	private String groupName;
	private int numberOfThreads;
	private int port;
	private int batchSize;
	private int msgSize;
	private int iterations;
	private ZoneRegistry registry;
	private Zone zone;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		zone.close();
		registry.close();
		Thread.sleep(3000);
	}

	private void initRegistry() throws IOException {
		registry = new ZoneRegistry();
		registry.setLocalhost("127.0.0.1");
		registry.setZoneName(groupName);
		registry.setPort(port);
		registry.start();
	}

	@Test
	public void testXSMsgWithSingleListener() throws IOException, InterruptedException {
		this.groupName = ZONE_NAME;
		this.numberOfThreads = 1;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(XS_PAYLOAD_SIZE), XS_PAYLOAD_SIZE + 64);
	}

	@Test
	public void testXSMsgWithTwoListener() throws IOException, InterruptedException {
		this.groupName = ZONE_NAME;
		this.numberOfThreads = 2;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(XS_PAYLOAD_SIZE), XS_PAYLOAD_SIZE + 64);
	}

	@Test
	public void testSMsgWithSingleListener() throws IOException, InterruptedException {
		this.groupName = ZONE_NAME;
		this.numberOfThreads = 1;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(S_PAYLOAD_SIZE), S_PAYLOAD_SIZE + 64);
	}

	@Test
	public void testSMsgWithTwoListener() throws IOException, InterruptedException {
		this.groupName = ZONE_NAME;
		this.numberOfThreads = 2;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(S_PAYLOAD_SIZE), S_PAYLOAD_SIZE + 64);
	}

	@Test
	public void testMMsgWithSingleListener() throws IOException, InterruptedException {
		this.groupName = ZONE_NAME;
		this.numberOfThreads = 1;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(M_PAYLOAD_SIZE), M_PAYLOAD_SIZE + 64);
	}

	@Test
	public void testMMsgWithTwoListener() throws IOException, InterruptedException {
		this.groupName = ZONE_NAME;
		this.numberOfThreads = 2;
		this.port = DEF_PORT;
		this.batchSize = DEF_BATCH_SIZE;
		this.iterations = DEF_ITERATIONS;
		doTest(createDataSize(M_PAYLOAD_SIZE), M_PAYLOAD_SIZE + 64);
	}

	public void doTest(String payload, int maxMessageInBytes) throws IOException, InterruptedException {
		initRegistry();
		final AtomicBoolean receiving = new AtomicBoolean(false);
		List<PerfItem> perfs = Lists.newArrayList();
		zone = new DirectZone(registry, numberOfThreads);
		zone.bind(new Serializer<String>() {

			@Override
			public byte[] serialize(String payload) {
				return payload.getBytes();
			}

			@Override
			public String deserialize(byte[] bytes) {
				return new String(bytes);
			}

			@Override
			public Class<String> getHandledType() {
				return String.class;
			}
		}, new PayloadHandler<String>() {

			@Override
			public void handle(String m) {
				if (receiving.compareAndSet(false, true)) {
					collectWatch.start();
				}
				emitLatch.countDown();

			}
		});
		zone.start();
		for (int iter = 0; iter < iterations; iter++) {
			emitLatch = new CountDownLatch(batchSize);
			this.msgSize = payload.length();
			emitWatch.start();
			for (int batchItem = 0; batchItem < batchSize; batchItem++) {
				zone.emit(payload, ROUTING_KEY, RoutingStrategy.ROUND_ROBIN);
			}
			emitWatch.stop();
			emitLatch.await();
			collectWatch.stop();
			perfs.add(new PerfItem(emitWatch.elapsedMillis(), collectWatch.elapsedMillis(), batchSize, msgSize));
			emitWatch.reset();
			collectWatch.reset();
			receiving.set(false);
		}
		printStats(perfs, DEF_ITERATIONS);
		System.gc();
		if (SLEEP_BETWEEN_ITERATION)
			Thread.sleep(3000);
	}

	private void printStats(List<PerfItem> items, int iterations) {
		printBanner();
		PerfItem worst = max(items);
		printItem("WORST PERFORMER", worst);
		PerfItem best = min(items);
		printItem("BEST PERFORMER", best);
		items.remove(worst);
		items.remove(best);
		PerfItem median = median(items);
		printItem("MEDIAN", median);
	}

	private void printItem(String measure, PerfItem item) {
		System.out.printf("%n%-40s%n", measure);
		System.out.println("                     ---------------------------------------------------------------------------------");
		System.out.printf("%20s %-28s %-31s %-30s%n", "", "Elapsed Time (ms)", "Throughput (msgs/s)", "Throughput (Mb/s)");
		System.out.println("------------------------------------------------------------------------------------------------------");
		System.out.printf("%15s %9s %12s %15s %15s %15s %15s%n", "Total msgs", "Send", "Receive", "Send", "Receive", "Send", "Receive");
		System.out.println("------------------------------------------------------------------------------------------------------");
		System.out.printf("%15d %9d %12d %15.3f %15.3f %15.3f %15.3f%n", item.batchSize, item.emitMs, item.collectMs, item.emitTpt, item.collectTpt,
				item.emitMb, item.collectMb);
		System.out.println("------------------------------------------------------------------------------------------------------");
	}

	private void printBanner() {
		System.out.println("\n------------------------------------------------------------------------------------------------------");
		System.out.printf("%d ITERATIONS of %d MESSAGES (%d bytes) with %d LISTENER(s)%n", iterations, batchSize, msgSize, numberOfThreads);
		System.out.println("------------------------------------------------------------------------------------------------------");
	}

	private PerfItem max(List<PerfItem> items) {
		Collections.sort(items);
		return items.get(items.size() - 1);
	}

	private PerfItem min(List<PerfItem> items) {
		Collections.sort(items);
		return items.get(0);
	}

	private PerfItem median(List<PerfItem> values) {
		if (values.size() % 2 == 1)
			return values.get((values.size() + 1) / 2 - 1);
		else {
			PerfItem lower = values.get(values.size() / 2 - 1);
			PerfItem upper = values.get(values.size() / 2);

			return new PerfItem((lower.emitMs + upper.emitMs) / 2, (lower.collectMs + upper.collectMs) / 2, lower.batchSize, lower.msgSize);
		}
	}

	private static final class PerfItem implements Comparable<PerfItem> {
		long emitMs;
		long collectMs;
		double emitTpt;
		double collectTpt;
		double emitMb;
		double collectMb;
		int batchSize;
		int msgSize;

		PerfItem(long sendWatchMs, long recvWatchMs, int batchSize, int msgSize) {
			this.emitMs = sendWatchMs;
			this.collectMs = recvWatchMs;
			this.emitTpt = ((double) batchSize) / emitMs * SECOND_2MS;
			this.collectTpt = ((double) batchSize) / collectMs * SECOND_2MS;
			this.emitMb = (emitTpt * ((double) msgSize) * 8 / M);
			this.collectMb = (collectTpt * ((double) msgSize) * 8 / M);
			this.batchSize = batchSize;
			this.msgSize = msgSize;
		}

		@Override
		public int compareTo(PerfItem o) {
			return Long.valueOf(emitMs + collectMs).compareTo(Long.valueOf(o.emitMs + o.collectMs));
		}

	}

	private static String createDataSize(int msgSize) {
		StringBuilder sb = new StringBuilder(msgSize);
		for (int i = 0; i < msgSize; i++) {
			sb.append('a');
		}
		return sb.toString();
	}

}
